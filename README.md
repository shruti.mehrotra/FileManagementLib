# Project with customized in-memory spring batch, file snapshot, file gen and file loader with custom templates
# How to configure the file status management library in your application?
1. Create a lib folder in your project root.
2. Copy the provided jar in the lib folder
3. Include the following dependency in your pom if it is a maven project
	<dependency>
			<groupId>FileManagementLib</groupId>
			<artifactId>FileManagementLib</artifactId>
			<version>0.0.1-SNAPSHOT</version>
			<scope>system</scope>
    		<systemPath>${project.basedir}/lib/FileManagementLib-0.0.1-SNAPSHOT-jar-with-dependencies.jar</systemPath>
	</dependency>
4. Import the configuration resource file in your startup class
	@Import(AppConfig.class)
5. Add the following package to your component scan
	com.fc
6. Integrate calls to the file status management utilities like snapshot, file gen, etc
7. Introduce the following properties if using the cleanable in-memory spring batch
	
	batchjob.retry.count=3
	task.backoff.interval=1000
	task.backoff.multiplier=2.0
	spring.batch.initializer.enabled= false

8. Include the following properties for connecting to the fileManagement database
	database.driverClassName=com.mysql.jdbc.Driver

	database.host=
	database.port=3306
	
	database.userName=
	database.password=
	database.name=klickpay
	
	datasource.initialSize=10
	datasource.maxActive=30
9. Start the application