CREATE DATABASE  IF NOT EXISTS `klickpay` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klickpay`;
-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: klickpay
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FILE_CONFIG_SNAPSHOT`
--

DROP TABLE IF EXISTS `FILE_CONFIG_SNAPSHOT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FILE_CONFIG_SNAPSHOT` (
  `FILE_CONFIG_ID` varchar(255) NOT NULL,
  `FILE_CONFIG_TYPE` varchar(150) NOT NULL,
  `FILE_TEMPLATE` blob NOT NULL,
  `FILE_PATH_REGEX` varchar(250) NOT NULL,
  `FILE_NAME_REGEX` varchar(250) NOT NULL,
  `FILE_EXTN` varchar(10) NOT NULL,
  `FILE_GEN_TYPE` varchar(100) NOT NULL,
  `DESTIN_TYPE` varchar(50) NOT NULL,
  `DESTIN_CONFIG` varchar(1000) NOT NULL,
  `CREATE_TS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FILE_CONFIG_ID`),
  UNIQUE KEY `FILE_CONFIG_TYPE` (`FILE_CONFIG_TYPE`),
  UNIQUE KEY `FILE_CONFIG_TYPE_UN` (`FILE_CONFIG_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FILE_CONFIG_SNAPSHOT`
--

LOCK TABLES `FILE_CONFIG_SNAPSHOT` WRITE;
/*!40000 ALTER TABLE `FILE_CONFIG_SNAPSHOT` DISABLE KEYS */;
INSERT INTO `FILE_CONFIG_SNAPSHOT` VALUES ('09a0ff5c-9a8d-4bb1-9178-a4ca4a137639','REFUND_ICICI','{ \"dbConfig\": { \"dbName\": \"klickpay\", \"host\": \"localhost\",\"userName\":\"root\",\"password\":\"root\" }, \"fileHeader\": { \"type\": \"FILE\" }, \"batches\": [ { \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"fileId\" }, { \"id\": \"col2\", \"name\": \"fileName\" }, { \"id\": \"col3\", \"name\": \"status\" } ], \"type\": \"BATCH\" }, \"body\": { \"tableName\": [ \"TXN\" ], \"whereClause\": \"WHERE 1=1\", \"columns\": [ { \"id\": \"col1\", \"name\": \"FILE_ID\" }, { \"id\": \"col2\", \"name\": \"COL1\" }, { \"id\": \"col3\", \"name\": \"STATUS\" } ], \"orderByClause\": \"ORDER BY STATUS desc\" } }, { \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"fileId\" }, { \"id\": \"col2\", \"name\": \"fileName\" }, { \"id\": \"col3\", \"name\": \"status\" } ], \"type\": \"BATCH\" } } ] }','conf/<ddMMyyyy>/ICICI','<FILE_TYPE>_icici_<ddMMyyyyhhmmss>','CSV','OUTBOUND','S3','test config','2017-04-06 09:01:43','2017-04-06 10:42:33');
/*!40000 ALTER TABLE `FILE_CONFIG_SNAPSHOT` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 19:10:19
