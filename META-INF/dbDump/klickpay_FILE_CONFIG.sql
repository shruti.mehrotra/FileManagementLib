CREATE DATABASE  IF NOT EXISTS `klickpay` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klickpay`;
-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: klickpay
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FILE_CONFIG`
--

DROP TABLE IF EXISTS `FILE_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FILE_CONFIG` (
  `FILE_CONFIG_ID` bigint(128) NOT NULL,
  `FILE_CONFIG_TYPE` varchar(150) NOT NULL,
  `FILE_TEMPLATE` blob NOT NULL,
  `FILE_PATH_REGEX` varchar(250) NOT NULL,
  `FILE_NAME_REGEX` varchar(250) NOT NULL,
  `FILE_EXTN` varchar(10) NOT NULL,
  `FILE_FLOW_TYPE` varchar(100) NOT NULL,
  `DESTIN_TYPE` varchar(50) NOT NULL,
  `CREATE_TS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `DESTIN_CONFIG_FN` varchar(1000) NOT NULL,
  `NOTIFICATION_TYPE` varchar(50) DEFAULT NULL,
  `NOTIFICATION_CONFIG` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`FILE_CONFIG_ID`),
  UNIQUE KEY `FILE_CONFIG_TYPE` (`FILE_CONFIG_TYPE`),
  UNIQUE KEY `FILE_CONFIG_TYPE_UN` (`FILE_CONFIG_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FILE_CONFIG`
--

LOCK TABLES `FILE_CONFIG` WRITE;
/*!40000 ALTER TABLE `FILE_CONFIG` DISABLE KEYS */;
INSERT INTO `FILE_CONFIG` VALUES (137113753134480196,'REFUND_HDFC','{ \"dbConfig\": { \"dbName\": \"klickpay\", \"host\": \"localhost\", \"userName\":\"root\", \"password\":\"root\" }, \"fileHeader\": { \"type\": \"FILE\",\"columns\":[{\"id\":\"col1\", \"name\":\"fileId\"}, {\"id\":\"col2\",\"name\":\"Col1\"},{\"id\":\"col3\",\"name\":\"Status\"}] }, \"batches\": [ { \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"FILE_ID\" }, { \"id\": \"col2\", \"name\": \"STATUS\" }, { \"id\": \"col3\", \"name\": \"COL1\" } ], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\":[ {\"key\":\"TXN\", \"value\":\"txn\",\"operator\":\"AS\"} ] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\":[ {\"key\":\"STATUS\", \"value\":\"\'DISABLED\'\",\"operator\":\"EQUALS\"}, {\"key\":\"COL1\", \"value\":\"\'test\'\",\"operator\":\"NOT_EQUALS\"} ] } }, \"columns\": [ { \"id\": \"col1\", \"name\": \"FILE_ID\" }, { \"id\": \"col2\", \"name\": \"COL1\" }, { \"id\": \"col3\", \"name\": \"STATUS\" } ], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\":[ {\"key\":\"STATUS\", \"value\":\"DESC\",\"operator\":\"SPACE\"} ] } } } }, { \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"fileId\" }, { \"id\": \"col2\", \"name\": \"fileName\" }, { \"id\": \"col3\", \"name\": \"status\" } ], \"type\": \"BATCH\" } } ] }','conf/<ddMMyyyy>/HDFC','<FILE_TYPE>_hdfc_<ddMMyyyyhhmmss>','CSV','OUTBOUND','S3','2017-04-13 11:21:53','2017-05-06 09:37:52','s3',NULL,NULL),(137119791825970016,'REFUND_MIGS','{ \"dbConfig\": { \"dbName\": \"klickpay\", \"host\": \"localhost\", \"userName\": \"root\", \"password\": \"root\" }, \"fileHeader\": { \"type\": \"FILE\", \"columns\": [{ \"id\": \"col1\", \"name\": \"transactionId\" }, { \"id\": \"col2\", \"name\": \"refId\" }, { \"id\": \"col3\", \"name\": \"idempotencyId\" }] }, \"batches\": [{ \"batchHeader\": { \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\" }] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"200\", \"operator\": \"GREATER_THAN\" }] } }, \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"DESC\", \"operator\": \"SPACE\" }] } } } }] }','conf/<ddMMyyyy>/migs','<FILE_TYPE>_<ddMMyyyyhhmmss>','CSV','OUTBOUND','S3','2017-04-20 11:06:34','2017-05-06 09:37:52','s3',NULL,NULL),(137121561749050038,'REFUND_KOTAK','{\"dbName\": \"klickpay\", \"batches\": [{\"batchName\":\"GT200\", \"batchHeader\": { \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\",\"pairSeparator\":null }] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"200\", \"operator\": \"GREATER_THAN\",\"pairSeparator\":\"AND\" }] } }, \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"DESC\", \"operator\": \"SPACE\",\"pairSeparator\":null }] } } }}, {\"batchName\":\"LT200\", \"batchHeader\": { \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\",\"pairSeparator\":null }] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"200\", \"operator\": \"LESS_THAN\",\"pairSeparator\":\"AND\" }] } }, \"columns\": [{ \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" }], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [{ \"key\": \"refund_amount\", \"value\": \"DESC\", \"operator\": \"SPACE\",\"pairSeparator\":null }] } } } }] }','conf/<ddMMyyyy>/kotak','<FILE_TYPE>_<ddMMyyyyhhmmss>','XLS','OUTBOUND','S3','2017-04-22 12:16:15','2017-05-06 09:37:52','s3',NULL,NULL),(137136155316450038,'REFUND_ICICI','{ \"dbName\": \"klickpay\", \"batches\": [ { \"batchName\": \"Sheet0\", \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"Bank Reference No.\" }, { \"id\": \"col2\", \"name\": \"Transaction Date\" }, { \"id\": \"col3\", \"name\": \"Transaction Amount\" }, { \"id\": \"col4\", \"name\": \"Refund Amount\" }, { \"id\": \"col5\", \"name\": \"Transaction Id\" }, { \"id\": \"col6\", \"name\": \"Reversal/Cancellation\" }, { \"id\": \"col7\", \"name\": \"Remarks\" } ], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\", \"pairSeparator\": \"NATURAL_JOIN\" }, { \"key\": \"payment_transaction\", \"value\": \"payment_txn\", \"operator\": \"AS\", \"pairSeparator\": null } ] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_txn.queue_status\", \"value\": \"\'REFUND_QUEUED\'\", \"operator\": \"EQUALS\", \"pairSeparator\": \"AND\" }, { \"key\": \"payment_txn.payment_type_code\", \"value\": \"\'NB\'\", \"operator\": \"EQUALS\", \"pairSeparator\": \"AND\" } ] } }, \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"payment_txn.created_time\", \"value\": \"DESC\", \"operator\": \"SPACE\", \"pairSeparator\": null } ] } }, \"onClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"payment_txn.reference_id\", \"value\": \"refund_txn.reference_id\", \"operator\": \"EQUALS\", \"pairSeparator\": null } ] } }, \"columns\": [ { \"id\": \"col1\", \"name\": \"payment_txn.pg_txn_id\" }, { \"id\": \"col2\", \"name\": \"payment_txn.n_created\" }, { \"id\": \"col3\", \"name\": \"payment_txn.amount\" }, { \"id\": \"col4\", \"name\": \"refund_txn.refund_amount\" }, { \"id\": \"col5\", \"name\": \"payment_txn.reference_id\" }, { \"id\": \"col6\", \"name\": \"Reversal\", \"valueType\": \"STATIC\" }, { \"id\": \"col7\", \"name\": \"payment_txn.merchant_transaction_id\" } ] } }], \"postUploadDBUpdate\":{ \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\", \"pairSeparator\": null } ] } }, \"whereClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_txn.FILE_ID\", \"value\": \"?FILE_ID\", \"operator\": \"EQUALS\", \"pairSeparator\": null } ] } }, \"setClause\":{ \"keyValWithDbOperatorList\":{ \"keyValPairWithDbOperatorList\":[ { \"key\":\"queue_status\", \"value\":\"\'REFUND_PROCESSED\'\", \"operator\":\"EQUALS\", \"pairSeparator\":\"COMMA\" }, { \"key\":\"updated_on\", \"value\":\"?REFUNDED_ON\", \"operator\":\"EQUALS\", \"pairSeparator\":\"COMMA\" }, { \"key\":\"refund_status\", \"value\":\"true\", \"operator\":\"EQUALS\", \"pairSeparator\":null } ] } } } }','conf/icici','<request.merchantId>_icici_nb_refunds_<yyyyMMdd>','XLS','OUTBOUND','S3','2017-05-09 09:38:51','2017-05-15 05:58:06','s3','EMAIL','{\"to\":\"shrutimehrotra7@gmail.com\",\"from\":\"fc@test.com\"}');
/*!40000 ALTER TABLE `FILE_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 19:10:19
