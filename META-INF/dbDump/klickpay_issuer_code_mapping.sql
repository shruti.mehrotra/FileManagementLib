CREATE DATABASE  IF NOT EXISTS `klickpay` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klickpay`;
-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: klickpay
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `issuer_code_mapping`
--

DROP TABLE IF EXISTS `issuer_code_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issuer_code_mapping` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `bank_code` varchar(50) DEFAULT NULL,
  `bank_name` varchar(200) DEFAULT NULL,
  `added_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bank_code` (`bank_code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issuer_code_mapping`
--

LOCK TABLES `issuer_code_mapping` WRITE;
/*!40000 ALTER TABLE `issuer_code_mapping` DISABLE KEYS */;
INSERT INTO `issuer_code_mapping` VALUES (1,'HDFC','HDFC Bank',NULL),(2,'IDBI','IDBI Bank',NULL),(3,'PNB','Punjab National Bank',NULL),(4,'CORPORATION','Corporation Bank',NULL),(5,'UBI','Union Bank Of India',NULL),(6,'ANDHRA','Andhra Bank',NULL),(7,'CBI','Central Bank of India',NULL),(8,'CANARA','Canara Bank',NULL),(9,'VIJAYA','Vijaya Bank',NULL),(10,'PMB','Punjab & Maharashtra Cooperative Bank',NULL),(11,'BOI','Bank Of India',NULL),(12,'ICICI','ICICI Bank',NULL);
/*!40000 ALTER TABLE `issuer_code_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 19:10:19
