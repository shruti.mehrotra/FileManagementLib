package com.freecharge.file.snapshot;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import java.util.List;

import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.type.FileConfigType;

public interface IFileSnapshotFetcherService {
	public FileSnapshotAndConfigResponse getFileStateAndConfigByFileId(long fileId);
	
	FileConfigSnapshotDto getFileConfigByFileConfigType(FileConfigType fileConfigType);
	
	FileSnapshotDto getByRecFilterHash(String hash);

	List<FileSnapshotDto> getByUpdateTsRange(String startUpdateTs, String endUpdateTs, FileConfigType fileConfigType);

	List<FileSnapshotDto> getFileManagementDtoListByConfigType(FileConfigType fileConfigType);

	List<FileSnapshotDto> getFileManagementDtoListByConfigTypeWithGenCompleteOrUploadFail(FileConfigType fileConfigType);
}
