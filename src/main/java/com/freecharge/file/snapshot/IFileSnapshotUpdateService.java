package com.freecharge.file.snapshot;

import com.freecharge.model.request.FileConfigRequest;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileStatus;

/**
 * 
 * @author shruti.mehrotra
 *
 */

public interface IFileSnapshotUpdateService {
	public boolean updateFileStatus(FileStatus fileStatus, long fileId);
	
	public boolean updateFileStatusAndNumOfRec(FileStatus fileStatus, long fileId, long numOfRecords);
	
	public boolean updateFileUrlAndStatus(FileStatus fileStatus, long fileId, String fileName);
	
	public FileGenResponse insert(FileConfigRequest request);

	long insertFileManagement(FileConfigType fileConfigType, long fileConfigId, FileStatus fileStatus, String hash, String fileUrl);
}
