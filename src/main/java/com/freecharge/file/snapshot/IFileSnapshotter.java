package com.freecharge.file.snapshot;

import com.freecharge.file.IFileStepValidator;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.response.ServiceResponse;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileSnapshotter extends IFileStepValidator {
	public ServiceResponse doSnapshot(FileSnapshotRequest request);

	public FileSnapshotDto getByRecFilterHash(String hash);
}
