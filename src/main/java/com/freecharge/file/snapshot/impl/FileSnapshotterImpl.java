package com.freecharge.file.snapshot.impl;

import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.DBSnapshotterDto;
import com.freecharge.dao.service.IDBUpdateService;
import com.freecharge.exceptions.ExceptionErrorCode;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.exceptions.InvalidFileStatusException;
import com.freecharge.exceptions.ValidationException;
import com.freecharge.file.init.IFileInitService;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.snapshot.IFileSnapshotUpdateService;
import com.freecharge.file.snapshot.IFileSnapshotter;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileStatus;
import com.freecharge.parser.FileTemplateParser;
import com.freecharge.util.FileGenUtil;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Slf4j
@Component("FileSnapshotterImpl")
public class FileSnapshotterImpl implements IFileSnapshotter {

	@Autowired
	@Qualifier("FileInitServiceImpl")
	private IFileInitService fileInitService;

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;

	@Autowired
	@Qualifier("DBUpdateServiceImpl")
	private IDBUpdateService dbUpdateService;

	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotUpdateService;

	@Autowired
	private FileTemplateParser templateParser;

	@Override
	public ServiceResponse doSnapshot(FileSnapshotRequest request) {
		ServiceResponse serviceResponse = new ServiceResponse();
		long fileId = 0;
		boolean snapshotSuccess = false;
		try {
			fileId = fileInitService.generateFileIdForRequest(request);
			// Fetch the snapshot and template info
			FileSnapshotAndConfigResponse response = snapshotFetcherService.getFileStateAndConfigByFileId(fileId);
			snapshotSuccess = isAlreadyComplete(response.getFileSnapshotDto().getFileStatus());
			if (!snapshotSuccess) {
				serviceResponse = proceed(response, request);
				snapshotSuccess = serviceResponse.isSuccess();
			}
		} catch (FileManagementLibException e1) {
			log.error("Error occurred while taking file snapshot for the request: {}. Error: {}", request.toString(), e1.getMessage());
			serviceResponse = ResponseGenUtil.getFailureServiceResponse(e1);
		} catch (Exception e) {
			log.error("Unknown error occurred while taking file snapshot for the request: {}. Error: {}", request.toString(), e);
			serviceResponse = ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException(e));
		} finally {
			if (!snapshotSuccess) {
				snapshotUpdateService.updateFileStatus(FileStatus.SNAPSHOT_FAILED, fileId);
				log.info("File status updated to {} for the fileId: {}!", FileStatus.SNAPSHOT_FAILED.name(),
						fileId);
			}
		}
		return serviceResponse;
	}

	private DBQueryDto mergeDtoClausesForSnapshot(DBQueryDto consolidatedDto, DBQueryDto dtoToMerge) {
		String[] mergedColToSelect = mergeStringArrays(consolidatedDto.getColumnsToSelect(),
				dtoToMerge.getColumnsToSelect());
		consolidatedDto.getTableNameClause().getKeyValPairs().getKeyValPairWithDbOperatorList()
				.addAll(dtoToMerge.getTableNameClause().getKeyValPairs().getKeyValPairWithDbOperatorList());
		consolidatedDto.getOnClause().getKeyValPairs().getKeyValPairWithDbOperatorList()
				.addAll(dtoToMerge.getOnClause().getKeyValPairs().getKeyValPairWithDbOperatorList());
		consolidatedDto.getWhereClause().getKeyValPairs().getKeyValPairWithDbOperatorList()
				.addAll(dtoToMerge.getWhereClause().getKeyValPairs().getKeyValPairWithDbOperatorList());
		consolidatedDto.setColumnsToSelect(mergedColToSelect);
		return consolidatedDto;
	}

	private String[] mergeStringArrays(String[] arr1, String[] arr2) {
		return ArrayUtils.addAll(arr1, arr2);
	}
	
	public ServiceResponse proceed(FileSnapshotAndConfigResponse response, FileSnapshotRequest request) throws IOException {
		ServiceResponse serviceResponse = new ServiceResponse();
		long fileId = response.getFileSnapshotDto().getFileId();
		String fileTemplateStr = response.getFileConfigSnapshotDto().getFileTemplate();
		long numOfRecUpdated = 0;
		FileTemplate fileTemplate = null;
		validateFileStatus(response.getFileSnapshotDto().getFileStatus());

		snapshotUpdateService.updateFileStatus(FileStatus.SNAPSHOT_START, fileId);
		log.info("File status updated to {} successfully for the fileId: {}!", FileStatus.SNAPSHOT_START.name(),
				fileId);

		fileTemplate = templateParser.getFileTemplateObjFromStr(fileTemplateStr, fileId, request);
		// Array is for different batches in the file template
		DBQueryDto[] dtoArr = fileTemplate.toDBRetrievalDto();
		DBQueryDto consolidatedDto = null;
		for (DBQueryDto dtoToMerge : dtoArr) {
			if (dtoToMerge != null) {
				// Initiate snapshot using configuration information
				if (consolidatedDto == null) {
					consolidatedDto = dtoToMerge;
				} else {
					consolidatedDto = mergeDtoClausesForSnapshot(consolidatedDto, dtoToMerge);
				}
			}
		}
		DBSnapshotterDto snapsotDto = new DBSnapshotterDto(consolidatedDto);
		snapsotDto.setColumnToUpdate(FileGenUtil.getFileIdDtoList(fileId));
		numOfRecUpdated = dbUpdateService.snapshotUpdate(snapsotDto, response, request.getFilterForSnapshotRecords());
		log.info("Snapshot done successfully for object: {}!", consolidatedDto.toString());
		FileStatus fileStatus = FileStatus.SNAPSHOT_COMPLETE;
		if (numOfRecUpdated <= 0) {
			fileStatus = FileStatus.SNAPSHOT_NO_RECORD;
		}

		snapshotUpdateService.updateFileStatusAndNumOfRec(fileStatus, fileId, numOfRecUpdated);
		log.info("File status updated to {} successfully for the fileId: {}!", fileStatus, fileId);

		serviceResponse = ResponseGenUtil
				.getSuccessServiceResponse("Snapshot done successfully for " + numOfRecUpdated + " records!");
		return serviceResponse;
	}
	
	@Override
	public FileSnapshotDto getByRecFilterHash(String hash) {
		FileSnapshotDto dto = snapshotFetcherService.getByRecFilterHash(hash);
		if (dto == null) {
			throw new ValidationException("No entry exists in file management db for the hash: " + hash,
					ExceptionErrorCode.INVALID_REC_FILTER_HASH);
		}
		return dto;
	}

	@Override
	public void validateFileStatus(FileStatus fileStatus) {
		if (fileStatus != FileStatus.INIT && fileStatus != FileStatus.SNAPSHOT_FAILED) {
			throw new InvalidFileStatusException("Invalid state for file snapshot: " + fileStatus);
		}

	}
	
	@Override
	public boolean isAlreadyComplete(FileStatus fileStatus) {
		return fileStatus == FileStatus.SNAPSHOT_COMPLETE || fileStatus.getIntValue() >= FileStatus.GEN_START.getIntValue();
	}
}
