package com.freecharge.file.snapshot.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.dao.entity.FileConfigEntity;
import com.freecharge.dao.entity.FileManagementEntity;
import com.freecharge.dao.mapper.FileConfigEntityRepo;
import com.freecharge.dao.mapper.FileManagementEntityRepo;
import com.freecharge.exceptions.FileConfigNotFoundException;
import com.freecharge.exceptions.InvalidFileIdException;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.type.FileConfigType;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileSnapshotFetcherServiceImpl")
public class FileSnapshotFetcherServiceImpl implements IFileSnapshotFetcherService {

	@Autowired
	private FileManagementEntityRepo snapshotEntityMapper;

	@Autowired
	private FileConfigEntityRepo configSnapshotEntityMapper;

	@Override
	public FileSnapshotAndConfigResponse getFileStateAndConfigByFileId(long fileId) {
		FileSnapshotAndConfigResponse response = new FileSnapshotAndConfigResponse();
		FileManagementEntity snapshotEntity = snapshotEntityMapper.getByFileId(fileId);
		FileConfigEntity configSnapshotEntity = null;
		if (snapshotEntity != null) {
			configSnapshotEntity = configSnapshotEntityMapper.getByFileConfigId(snapshotEntity.getFileConfigId());
			response.setFileSnapshotDto(snapshotEntity.toDto());
		} else {
			throw new InvalidFileIdException("Invalid file id: " + fileId);
		}

		if (configSnapshotEntity != null) {
			response.setFileConfigSnapshotDto(configSnapshotEntity.toDto());
		} else {
			throw new FileConfigNotFoundException("No configuration found for the file id: " + fileId
					+ "with configId: " + snapshotEntity.getFileConfigId());
		}

		return response;
	}
	
	@Override
	public FileConfigSnapshotDto getFileConfigByFileConfigType(FileConfigType fileConfigType) {
		FileConfigSnapshotDto response = new FileConfigSnapshotDto();
		FileConfigEntity configEntity = null;
		configEntity = configSnapshotEntityMapper.getByFileConfigType(fileConfigType.name());
		response = configEntity.toDto();
		
		if (response == null) {
			throw new FileConfigNotFoundException("No configuration found for the file type: " + fileConfigType.name());
		}
		

		return response;
	}
	
	@Override
	public List<FileSnapshotDto> getFileManagementDtoListByConfigType(FileConfigType fileConfigType) {
		List<FileSnapshotDto> response = new ArrayList<>();
		List<FileManagementEntity> entities = new ArrayList<>();
		entities = snapshotEntityMapper.getByFileConfigType(fileConfigType.name());
		for (FileManagementEntity entity: entities) {
			response.add(entity.toDto());
		}
		
		return response;
	}
	
	@Override
	public List<FileSnapshotDto> getFileManagementDtoListByConfigTypeWithGenCompleteOrUploadFail(FileConfigType fileConfigType) {
		List<FileSnapshotDto> response = new ArrayList<>();
		List<FileManagementEntity> entities = new ArrayList<>();
		entities = snapshotEntityMapper.getByConfigTypeWithGenCompleteOrUploadFail(fileConfigType.name());
		for (FileManagementEntity entity: entities) {
			response.add(entity.toDto());
		}
		
		return response;
	}

	@Override
	public FileSnapshotDto getByRecFilterHash(String hash) {
		FileManagementEntity entity = snapshotEntityMapper.getByRecFilterHash(hash);
		if (entity != null) {
			return entity.toDto();
		}
		return null;
	}
	
	@Override
	public List<FileSnapshotDto> getByUpdateTsRange(String startUpdateTs, String endUpdateTs, FileConfigType fileConfigType) {
		List<FileSnapshotDto> dtoList = new ArrayList<>();
		List<FileManagementEntity> entities = snapshotEntityMapper.getByUpdateTsRange(startUpdateTs, endUpdateTs, fileConfigType.name());
		for (FileManagementEntity entity : entities) {
			if (entity != null) {
				dtoList.add(entity.toDto());
			}
		}
		return dtoList;
	}

}
