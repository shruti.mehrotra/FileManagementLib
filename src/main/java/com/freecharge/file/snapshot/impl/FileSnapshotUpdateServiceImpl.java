package com.freecharge.file.snapshot.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.dao.entity.FileConfigEntity;
import com.freecharge.dao.entity.FileManagementEntity;
import com.freecharge.dao.mapper.FileConfigEntityRepo;
import com.freecharge.dao.mapper.FileManagementEntityRepo;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.file.snapshot.IFileSnapshotUpdateService;
import com.freecharge.model.request.FileConfigRequest;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileStatus;
import com.freecharge.util.ResponseGenUtil;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileSnapshotUpdateServiceImpl")
public class FileSnapshotUpdateServiceImpl implements IFileSnapshotUpdateService {

	@Autowired
	private FileManagementEntityRepo snapshotEntityMapper;
	
	@Autowired
	private FileConfigEntityRepo configSnapshotEntityMapper;

	@Override
	public boolean updateFileStatus(FileStatus fileStatus, long fileId) {
		boolean isSuccess = false;
		FileManagementEntity entity = new FileManagementEntity();
		entity.setFileId(fileId);
		entity.setFileStatus(fileStatus.name());
		snapshotEntityMapper.updateStatusByFileId(entity);
		return isSuccess;
	}

	@Override
	public boolean updateFileUrlAndStatus(FileStatus fileStatus, long fileId, String fileName) {
		boolean isSuccess = false;
		FileManagementEntity entity = new FileManagementEntity();
		entity.setFileId(fileId);
		entity.setFileStatus(fileStatus.name());
		entity.setFileName(fileName);
		snapshotEntityMapper.updateFileNameAndStatusByFileId(entity);
		return isSuccess;
	}

	@Override
	public FileGenResponse insert(FileConfigRequest request) {
		FileGenResponse response = new FileGenResponse();
		FileConfigEntity fileConfigSnapshot = request.toFileConfigSnapshotEntity();
		try {
			configSnapshotEntityMapper.insert(fileConfigSnapshot);
			//FileManagementEntity fileSnapshot = request.toFileSnapshotEntity();
			//snapshotEntityMapper.insert(fileSnapshot);
			response = ResponseGenUtil.getSuccessResponse(null, fileConfigSnapshot.getFileConfigId());
		} catch (Exception e) {
			response = ResponseGenUtil.getFailureResponse(null, null, new FileManagementLibException(e));
		}
		return response;
	}
	
	@Override
	public long insertFileManagement(FileConfigType fileConfigType, long fileConfigId, FileStatus fileStatus, String hash, String fileUrl) {
		FileManagementEntity fileManagement =  new FileManagementEntity(fileConfigType, fileConfigId, FileStatus.INIT, hash, fileUrl);
		snapshotEntityMapper.insert(fileManagement);
		return fileManagement.getFileId();
	}

	@Override
	public boolean updateFileStatusAndNumOfRec(FileStatus fileStatus, long fileId, long numOfRecords) {
		boolean isSuccess = false;
		FileManagementEntity entity = new FileManagementEntity();
		entity.setFileId(fileId);
		entity.setFileStatus(fileStatus.name());
		entity.setNumOfRecords(numOfRecords);
		snapshotEntityMapper.updateStatusAndNumOfRecByFileId(entity);
		return isSuccess;
	}


}
