package com.freecharge.file.updator.service;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.dao.dto.DBPostUploadUpdateDto;
import com.freecharge.dao.service.IDBUpdateService;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.exceptions.InvalidFileStatusException;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.snapshot.IFileSnapshotUpdateService;
import com.freecharge.file.updator.IPostUploadUpdateService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.dto.template.PostUploadDBUpdate;
import com.freecharge.model.request.RunUpdateRequest;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileStatus;
import com.freecharge.parser.FileTemplateParser;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Service("PostUploadUpdateServiceImpl")
@Slf4j
public class PostUploadUpdateServiceImpl implements IPostUploadUpdateService {

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcher;

	@Autowired
	FileTemplateParser fileTemplateParser;

	@Autowired
	@Qualifier("DBUpdateServiceImpl")
	IDBUpdateService dbUpdateService;

	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotUpdateService;

	@Override
	public ServiceResponse runUpdate(RunUpdateRequest request) {
		ServiceResponse response = new ServiceResponse();
		boolean updateSuccess = false;
		long fileId = request.getFileId();
		FileSnapshotAndConfigResponse fileResponse = snapshotFetcher.getFileStateAndConfigByFileId(fileId);

		validateFileStatus(fileResponse.getFileSnapshotDto().getFileStatus());

		updateSuccess = isAlreadyComplete(fileResponse.getFileSnapshotDto().getFileStatus());

		snapshotUpdateService.updateFileStatus(FileStatus.POST_UPLOAD_UPDATE_START, fileId);
		log.info("File status updated to {} successfully for the fileId: {}!",
				FileStatus.POST_UPLOAD_UPDATE_START.name(), fileId);

		FileTemplate fileTemplate = fileTemplateParser
				.getFileTemplateObjFromStr(fileResponse.getFileConfigSnapshotDto().getFileTemplate(), fileId, request);
		PostUploadDBUpdate dbUpdate = fileTemplate.getPostUploadDBUpdate();
		DBPostUploadUpdateDto dto = new DBPostUploadUpdateDto();
		try {
			dto.setDbConfig(fileTemplate.getDBConfigByDbName(fileTemplate.getDbName()));
			dto.setPostUploadDBUpdateDto(dbUpdate);
			if (dbUpdate != null) {
				dbUpdateService.postDBUpdate(dto, fileResponse);
			} else {
				response = ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException(
						"No postUploadDBUpdate clause configured for the request: " + request.toString()));
			}
		} catch (FileManagementLibException e) {
			response = ResponseGenUtil.getFailureServiceResponse(e);
		} catch (IOException | SQLException e) {
			response = ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException(e.getMessage(), e));
		} finally {
			if (!updateSuccess) {
				snapshotUpdateService.updateFileStatus(FileStatus.POST_UPLOAD_UPDATE_FAILED, fileId);
				log.info("File status updated to {} for the fileId: {}!", FileStatus.POST_UPLOAD_UPDATE_FAILED.name(),
						fileId);
			}
		}
		return response;

	}

	@Override
	public void validateFileStatus(FileStatus fileStatus) {
		if (fileStatus != FileStatus.UPLOAD_COMPLETE && fileStatus != FileStatus.POST_UPLOAD_UPDATE_FAILED) {
			throw new InvalidFileStatusException("Invalid state for post upload update: " + fileStatus);
		}

	}

	@Override
	public boolean isAlreadyComplete(FileStatus fileStatus) {
		return fileStatus == FileStatus.POST_UPLOAD_UPDATE_COMPLETE
				|| fileStatus.getIntValue() >= FileStatus.NOTIFICATION_START.getIntValue();
	}

}
