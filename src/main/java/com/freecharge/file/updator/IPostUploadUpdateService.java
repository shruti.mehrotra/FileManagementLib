package com.freecharge.file.updator;

import com.freecharge.model.request.RunUpdateRequest;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileStatus;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IPostUploadUpdateService {

	ServiceResponse runUpdate(RunUpdateRequest request);

	void validateFileStatus(FileStatus fileStatus);

	boolean isAlreadyComplete(FileStatus fileStatus);
}
