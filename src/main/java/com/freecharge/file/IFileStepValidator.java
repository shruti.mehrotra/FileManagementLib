package com.freecharge.file;

import com.freecharge.model.type.FileStatus;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileStepValidator {
	public void validateFileStatus(FileStatus fileStatus);
	
	boolean isAlreadyComplete(FileStatus fileStatus);
}
