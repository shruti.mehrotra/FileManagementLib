package com.freecharge.file.init;

import com.freecharge.file.IFileStepValidator;
import com.freecharge.model.request.FileSnapshotRequest;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileInitService extends IFileStepValidator {
	public long generateFileIdForRequest(FileSnapshotRequest request);
}
