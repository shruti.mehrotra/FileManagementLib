package com.freecharge.file.init.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.constants.FileRegexConstants;
import com.freecharge.dao.dto.KeyValDtoList;
import com.freecharge.file.init.IFileInitService;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.snapshot.impl.FileSnapshotUpdateServiceImpl;
import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.FileObject;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileStatus;
import com.freecharge.util.FileGenUtil;
import com.freecharge.util.HashUtil;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileInitServiceImpl")
public class FileInitServiceImpl implements IFileInitService {

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;
	
	@Autowired
	private FileSnapshotUpdateServiceImpl snapshotUpdateService;
	
	@Autowired
	private FileGenUtil fileGenUtil;
	
	@Override
	public long generateFileIdForRequest(FileSnapshotRequest request) {
		long fileId = 0;
		// Step 1: Fetch the configuration by configType
		FileConfigSnapshotDto configDto = snapshotFetcherService.getFileConfigByFileConfigType(request.getFileConfigType());
		// Step 2: Club the values in the request and generate a hash for the
		// same, store this id in the fileManagement table. This field should
		// have a unique constraint applied
		//return the file id
		String hash = HashUtil.getHashByFileManagementRequest(request);
		
		FileSnapshotDto dto = snapshotFetcherService.getByRecFilterHash(hash);
		
		
		if (dto == null) {
			String fileUrl = getFileUrl(configDto, request.getKeyValDtoList());
			fileId = snapshotUpdateService.insertFileManagement(request.getFileConfigType(), configDto.getFileConfigId(), FileStatus.INIT, hash, fileUrl);
		} else {
			fileId = dto.getFileId();
		}
		return fileId;
	}

	
	public String getFileUrl(FileConfigSnapshotDto dto, KeyValDtoList dtoList) {
		String fileUrl = null;
		FileObject fileObj = fileGenUtil.getFileObjectForRegexDto(dto, dtoList != null ? dtoList.getKeyValDtoList(): null);
		fileUrl = fileGenUtil.getCompleteFileName(fileObj);
		fileUrl = getSequencedFileUrlIfApplicable(fileUrl, dto.getFileConfigType());
		return fileUrl;
	}

	private String getSequencedFileUrlIfApplicable(String fileUrl, FileConfigType fileConfigType) {
		List<FileSnapshotDto> dtoList = fileGenUtil.getFileManagementDtoByFileType(fileConfigType);
		if (dtoList != null && !dtoList.isEmpty()) {
			int occuranceCount = 0;
			int dotIndex = fileUrl.lastIndexOf(FileRegexConstants.DOT);
			String fileNameWithoutExtn = fileUrl.substring(0, dotIndex);
			for (FileSnapshotDto dto: dtoList) {
				if (!StringUtils.isEmpty(dto.getFileName()) && dto.getFileName().contains(fileNameWithoutExtn)) {
					occuranceCount++;
				}
			}
			if (occuranceCount >= 1) {
				StringBuilder fileUrlBuilder = new StringBuilder();
				fileUrlBuilder.append(fileNameWithoutExtn).append(FileRegexConstants.HYPHEN).append(occuranceCount).append(fileUrl.substring(dotIndex));
				fileUrl = fileUrlBuilder.toString();
			}
		}
		return fileUrl;
	}

	@Override
	public boolean isAlreadyComplete(FileStatus fileStatus) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void validateFileStatus(FileStatus fileStatus) {
		
		
	}
}
