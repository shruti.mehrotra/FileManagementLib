package com.freecharge.file.notification.service.impl;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import org.springframework.stereotype.Service;

import com.freecharge.file.notification.service.INotificationService;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileConfigType;

@Service("NotificationServiceImpl")
public class NotificationServiceImpl implements INotificationService {

	@Override
	public ServiceResponse sendNotification(FileConfigType fileConfigType) {
		// TODO Send notification for all the types with file status as UPLOAD_COMPLETE
		return null;
	}

}
