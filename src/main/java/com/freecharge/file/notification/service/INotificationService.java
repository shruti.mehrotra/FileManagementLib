package com.freecharge.file.notification.service;

import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileConfigType;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface INotificationService {
	ServiceResponse sendNotification(FileConfigType fileConfigType);
}
