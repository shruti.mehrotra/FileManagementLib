package com.freecharge.file.uploader;

import java.util.List;

import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.response.FileUploadResponse;
import com.freecharge.model.type.DestinationType;

/**
 * 
 * @author shruti.mehrotra
 *
 */

public interface ITypeUploaderService {
	FileUploadResponse upload(List<FileSnapshotAndConfigResponse> dtoList);
	
	public DestinationType getDestinationType();
}
