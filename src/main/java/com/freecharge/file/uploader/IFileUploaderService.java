package com.freecharge.file.uploader;

import com.freecharge.model.response.FileUploadResponse;
import com.freecharge.model.type.FileConfigType;

/**
 * 
 * @author shruti.mehrotra
 *
 */

public interface IFileUploaderService {
	FileUploadResponse uploadFile(FileConfigType fileConfigType);
}
