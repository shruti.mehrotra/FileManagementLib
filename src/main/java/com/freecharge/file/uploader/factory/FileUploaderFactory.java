package com.freecharge.file.uploader.factory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.file.uploader.ITypeUploaderService;
import com.freecharge.model.type.DestinationType;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component
public class FileUploaderFactory {
	@Autowired
	List<ITypeUploaderService> fileUploadServiceList;

	Map<DestinationType, ITypeUploaderService> fileUploadServiceMap = new HashMap<>();

	@PostConstruct
	public void initBeans() {
		for (ITypeUploaderService fileUploadService : fileUploadServiceList) {
			if (fileUploadServiceMap.containsKey(fileUploadService.getDestinationType())) {
				throw new RuntimeException(
						"File uploader services with duplicate destination type cannot be initialized!");
			}
			fileUploadServiceMap.put(fileUploadService.getDestinationType(), fileUploadService);
		}
	}
	
	public ITypeUploaderService getFileUploaderByDestinType(DestinationType type) {
		if (!fileUploadServiceMap.containsKey(type)) {
			throw new RuntimeException("FileUploadService: " + type + " is not registered!");
		}
		return fileUploadServiceMap.get(type);
	}
}
