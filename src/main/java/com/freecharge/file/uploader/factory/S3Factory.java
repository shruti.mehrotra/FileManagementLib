package com.freecharge.file.uploader.factory;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.freecharge.model.dto.S3Config;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shruti.mehrotra
 *
 */
@Component
@Slf4j
public class S3Factory {

	private AmazonS3 s3SharedClient = null;

	public AmazonS3 getS3Client(S3Config s3ClientConfig) {

		log.debug("Fetching AmazonS3 Client Details...");
		if (!StringUtils.isEmpty(s3ClientConfig.getS3SharedAccessId())
				&& !StringUtils.isEmpty(s3ClientConfig.getS3SharedSecretKey())) {
			AWSCredentials s3Credentials = new BasicAWSCredentials(s3ClientConfig.getS3SharedAccessId(),
					s3ClientConfig.getS3SharedSecretKey());
			s3SharedClient = new AmazonS3Client(s3Credentials);
		} else {
			s3SharedClient = new AmazonS3Client(new InstanceProfileCredentialsProvider());
		}
		// set Region Name.
		s3SharedClient.setRegion(Region.getRegion(Regions.fromName(s3ClientConfig.getRegion())));
		return s3SharedClient;
	}
}
