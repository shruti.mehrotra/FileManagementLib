package com.freecharge.file.uploader.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.uploader.IFileUploaderService;
import com.freecharge.file.uploader.ITypeUploaderService;
import com.freecharge.file.uploader.factory.FileUploaderFactory;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.response.FileUploadResponse;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.DestinationType;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.util.FileGenUtil;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Service("FileUploaderServiceImpl")
@Slf4j
public class FileUploaderServiceImpl implements IFileUploaderService {

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotfetcherService;

	@Autowired
	private FileGenUtil fileGenUtil;

	@Autowired
	private FileUploaderFactory fileUploaderFactory;

	public FileUploadResponse uploadFile(FileConfigType fileConfigType) {
		FileUploadResponse uploadResponse = null;
		List<FileSnapshotDto> dtoList = snapshotfetcherService
				.getFileManagementDtoListByConfigTypeWithGenCompleteOrUploadFail(fileConfigType);
		List<FileSnapshotAndConfigResponse> s3DtoList = new ArrayList<>();
		for (FileSnapshotDto dto : dtoList) {
			FileSnapshotAndConfigResponse response = fileGenUtil.getSnapshotAndConfigByFileId(dto.getFileId());
			switch (response.getFileConfigSnapshotDto().getDestinationType()) {
			case S3:
				s3DtoList.add(response);
				break;
			default:
				log.error("Unsupported destination type: {}, in the file config: {}",
						response.getFileConfigSnapshotDto().getDestinationType(),
						response.getFileConfigSnapshotDto().toString());
				uploadResponse = new FileUploadResponse();
				Map<String, ServiceResponse> responseMap = new HashMap<>();
				responseMap.put("error", ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException(
						"Unsupported destination type:" + response.getFileConfigSnapshotDto().getDestinationType())));
				uploadResponse.setResponseMap(responseMap);
				break;
			}
		}

		if (!s3DtoList.isEmpty()) {
			ITypeUploaderService uploaderService = fileUploaderFactory.getFileUploaderByDestinType(DestinationType.S3);
			uploadResponse = uploaderService.upload(s3DtoList);
			log.info("S3 File upload response for the configType: {} is {}", fileConfigType, uploadResponse);
		}
		return uploadResponse;

	}
}
