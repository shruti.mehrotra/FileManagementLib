package com.freecharge.file.uploader.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.exceptions.InvalidFileStatusException;
import com.freecharge.file.generator.BaseFileGeneratorService;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.snapshot.IFileSnapshotUpdateService;
import com.freecharge.file.uploader.ITypeUploaderService;
import com.freecharge.file.uploader.factory.S3Factory;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.S3Config;
import com.freecharge.model.response.FileUploadResponse;
import com.freecharge.model.type.DestinationType;
import com.freecharge.model.type.FileStatus;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class S3UploaderServiceImpl implements ITypeUploaderService {

	@Autowired
	private S3Factory amazonS3Factory;


	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotfetcherService;
	
	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotUpdateService;

	@Override
	public FileUploadResponse upload(List<FileSnapshotAndConfigResponse> dtoList) {
		FileUploadResponse response = new FileUploadResponse();
		
		for (FileSnapshotAndConfigResponse dto : dtoList) {
			boolean uploadSuccess = false;
			validateFileStatus(dto.getFileSnapshotDto().getFileStatus());
			String inputFilePath = dto.getFileSnapshotDto().getFileName();

			long fileId = dto.getFileSnapshotDto().getFileId();
			try {
				snapshotUpdateService.updateFileStatus(FileStatus.UPLOAD_START, fileId);
				log.info("File status updated to {} successfully for the fileId: {}!", FileStatus.UPLOAD_START.name(),
						fileId);
				File tempFile = new File(BaseFileGeneratorService.TEMP_FOLDER + inputFilePath);
				S3Config s3Config = dto.getFileConfigSnapshotDto().getS3Config();
				String bucketName = s3Config.getBucketName();
				AmazonS3 amazonS3Client = amazonS3Factory.getS3Client(s3Config);
				PutObjectResult putObjectResult;
				putObjectResult = amazonS3Client.putObject(bucketName, inputFilePath, new FileInputStream(tempFile), null);
				log.info("Result for putting " + inputFilePath + " in bucket " + bucketName + " : "
						+ putObjectResult.toString());
				response.getResponseMap().put(String.valueOf(fileId),
						ResponseGenUtil.getSuccessServiceResponse("File uploaded successfully!"));
				log.info("Deleting the temp file: {}", tempFile.getName());
				tempFile.delete();
				snapshotUpdateService.updateFileStatus(FileStatus.UPLOAD_COMPLETE, fileId);
				log.info("File status updated to {} successfully for the fileId: {}!", FileStatus.UPLOAD_COMPLETE, fileId);
				uploadSuccess = true;
			} catch (Exception e) {
				response.getResponseMap().put(String.valueOf(fileId),
						ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException(e.getMessage())));
				log.error("Unknow error occured while uploading file for the file dto request: {}, Error: {}", dto.toString(), e);
			} finally {
				if (!uploadSuccess) {
					snapshotUpdateService.updateFileStatus(FileStatus.UPLOAD_FAILED, fileId);
					log.info("File status updated to {} successfully for the fileId: {}!", FileStatus.UPLOAD_FAILED, fileId);
				}
			}
			
		}

		return response;
	}
	
	public void validateFileStatus(FileStatus fileStatus) {
		if (fileStatus != FileStatus.GEN_COMPLETE) {
			throw new InvalidFileStatusException("Invalid state for file upload: " + fileStatus);
		}

	}

	@Override
	public DestinationType getDestinationType() {
		return DestinationType.S3;
	}

}
