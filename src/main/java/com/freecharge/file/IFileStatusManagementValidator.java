package com.freecharge.file;

import com.freecharge.model.request.ServiceRequest;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileStatusManagementValidator {
	public boolean validateRequest(ServiceRequest request);
}
