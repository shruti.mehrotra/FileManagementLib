package com.freecharge.file.generator.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.freecharge.constants.FileRegexConstants;
import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.DBQueryResponse;
import com.freecharge.exceptions.FileGenerationException;
import com.freecharge.file.generator.BaseFileGeneratorService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.dto.template.Footer;
import com.freecharge.model.dto.template.Header;
import com.freecharge.model.type.FileExtension;
import com.freecharge.util.AppUtil;
import com.freecharge.util.FileGenUtil;
import com.opencsv.CSVWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("CSVFileGeneratorImpl")
@Slf4j
public class CSVFileGeneratorImpl extends BaseFileGeneratorService {
	
	@Override
	public FileExtension getFileExtension() {
		return FileExtension.CSV;
	}

	@Override
	public boolean createFile(FileTemplate fileTemplate, String fileUrl, FileSnapshotAndConfigResponse response) {
		DBQueryResponse dbResponse = null;
		boolean fileCreated = false;
		// Create CSV file
		FileWriter writer = null;
		CSVWriter csvWriter = null;
		try {
			writer = new FileWriter(fileUrl);

			// using custom delimiter and quote character
			char fileDelimiter = fileTemplate.getFileDelimiter() == FileRegexConstants.SPACE_CHAR ? ',' : fileTemplate.getFileDelimiter();
			csvWriter = new CSVWriter(writer, fileDelimiter);
	
			List<String[]> header = getHeaderArray(fileTemplate.getFileHeader());
	
			DBQueryDto[] dbQueryDtoArray = fileTemplate.toDBRetrievalDto();

			csvWriter.writeAll(header);
			Long fileId = response.getFileSnapshotDto().getFileId();
			for (DBQueryDto dbQueryDto : dbQueryDtoArray) {
				if (dbQueryDto != null) {
					//Override the where clause.. hard code it to fetching all the records by file id
					dbQueryDto.setWhereClause(FileGenUtil.getWhereClauseByColumnMap(FileGenUtil.getFileIdDtoList(fileId)));
					dbResponse = retrievalService.retrieveData(dbQueryDto, response);
					csvWriter.writeAll(dbResponse.getRs(), false);
					csvWriter.writeAll(getFooterArray(dbQueryDto.getBatchFooter()));
				}
			}

			fileCreated = true;
		} catch (Exception e) {
			log.error(
					"Unknown error occured while generating xls for the template: {}, for the fileUrl: {}, and the configResponse: {}, Error: {}",
					fileTemplate.toString(), fileUrl, response.toString(), e.getMessage());
			throw new FileGenerationException("Got error while generating file with fileUrl: " + fileUrl, e);
		} finally {
			try {
				if (csvWriter != null) {
					csvWriter.close();
				}
			} catch (IOException e) {
				// cannot do anyhting
			}
			AppUtil.closeDBConnections(dbResponse);
		}

		return fileCreated;

	}

	private List<String[]> getHeaderArray(Header fileHeader) {
		List<String[]> header = new ArrayList<>();
		if (fileHeader != null) {
			String columnNames[] = FileGenUtil.toStringArray(fileHeader.getColumns());
			if (columnNames != null) {
				header.add(columnNames);
			}
		}
		return header;
	}
	
	private List<String[]> getFooterArray(Footer fileFooter) {
		List<String[]> footer = new ArrayList<>();
		if (fileFooter != null) {
			String[] columnNames = FileGenUtil.toStringArray(fileFooter.getColumns());
			if (columnNames != null) {
				footer.add(columnNames);
			}
		}
		return footer;
	}

}
