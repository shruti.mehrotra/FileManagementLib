package com.freecharge.file.generator.impl;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.WhereClause;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class BatchBucketizer {
	public static ResultSet bucketize(DBQueryDto dbQueryDto, ResultSet resultSet) {
		ResultSet bucketizedRs = null;
		WhereClause whereClause = dbQueryDto.getWhereClause();
		try {
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int numCols = resultSetMetaData.getColumnCount();
			while (resultSet.next()) {
				for (int i = 0; i < numCols; i++) {
					Object value = null;
					value = resultSet.getObject(i + 1);
					whereClause.applyAsJava();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bucketizedRs;
	}
}
