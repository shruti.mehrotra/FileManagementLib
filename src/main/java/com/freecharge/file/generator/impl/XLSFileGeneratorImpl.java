package com.freecharge.file.generator.impl;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;

import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.DBQueryResponse;
import com.freecharge.exceptions.FileGenerationException;
import com.freecharge.file.generator.BaseFileGeneratorService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.type.FileExtension;
import com.freecharge.util.AppUtil;
import com.freecharge.util.FileGenUtil;
import com.freecharge.util.ResultSetToExcel;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("XLSFileGeneratorImpl")
@Slf4j
public class XLSFileGeneratorImpl extends BaseFileGeneratorService {

	@Override
	public FileExtension getFileExtension() {
		return FileExtension.XLS;
	}

	@Override
	public boolean createFile(FileTemplate fileTemplate, String fileUrl, FileSnapshotAndConfigResponse response) {
		DBQueryResponse dbResponse = null;
		boolean fileCreated = false;
		if (response.getFileSnapshotDto().getNumOfRecords() > 0) {
			try {
				DBQueryDto[] dbQueryDtoArray = fileTemplate.toDBRetrievalDto();
				ResultSetToExcel resultSetToExcel = new ResultSetToExcel();
				resultSetToExcel.setWorkbook(new HSSFWorkbook());

				File file = new File(fileUrl);

				int sheetIdx = 1;
				Long fileId = response.getFileSnapshotDto().getFileId();
				for (DBQueryDto dbQueryDto : dbQueryDtoArray) {
					if (dbQueryDto != null) {
						String sheetName = null;
						if (!StringUtils.isEmpty(dbQueryDto.getBatchSheetName())) {
							sheetName = dbQueryDto.getBatchSheetName();
						} else {
							sheetName = "Sheet" + sheetIdx++;
						}
						log.info("Generating sheet: {} for the file: {}", sheetName, fileUrl);
						dbQueryDto.getWhereClause().getKeyValPairs().getKeyValPairWithDbOperatorList()
								.addAll(FileGenUtil.getFileIdDtoList(fileId));
						dbResponse = retrievalService.retrieveData(dbQueryDto, response);
						if (dbResponse != null) {
							resultSetToExcel.initialize(dbResponse.getRs(), dbQueryDto, sheetName);
							resultSetToExcel.generate(file);
						}
					}
				}

				fileCreated = true;
			} catch (Exception e) {
				log.error(
						"Unknown error occured while generating xls for the template: {}, for the fileUrl: {}, and the configResponse: {}, Error: {}",
						fileTemplate.toString(), fileUrl, response.toString(), e.getMessage());
				log.error("XLSFileGen Error description: ", e);
				throw new FileGenerationException("Got error while generating file with fileUrl: " + fileUrl, e);
			} finally {
				AppUtil.closeDBConnections(dbResponse);
			}
		}

		return fileCreated;

	}

}
