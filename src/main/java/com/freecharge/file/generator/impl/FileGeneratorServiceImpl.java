package com.freecharge.file.generator.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.exceptions.InvalidFileStatusException;
import com.freecharge.file.generator.FileGeneratorFactory;
import com.freecharge.file.generator.IFileGeneratorService;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.file.snapshot.IFileSnapshotter;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.request.FileGenRequest;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.response.FileGenListResponse;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.type.FileStatus;
import com.freecharge.util.FileGenUtil;
import com.freecharge.util.HashUtil;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileGeneratorServiceImpl")
@Slf4j
public class FileGeneratorServiceImpl implements IFileGeneratorService {
	@Autowired
	private FileGeneratorFactory fileGenFactory;

	@Autowired
	private FileGenUtil fileGenUtil;

	@Autowired
	@Qualifier("FileSnapshotterImpl")
	private IFileSnapshotter fileSnapshotter;

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;

	@Override
	public FileGenResponse generateFileBySnapshotRequest(FileSnapshotRequest request) {
		String hash = HashUtil.getHashByFileManagementRequest(request);
		Long fileId = null;
		String fileUrl = null;
		try {
			FileSnapshotDto dto = fileSnapshotter.getByRecFilterHash(hash);
			fileId = dto.getFileId();
			FileSnapshotAndConfigResponse response = fileGenUtil.getSnapshotAndConfigByFileId(fileId);
			if (!isAlreadyComplete(response.getFileSnapshotDto().getFileStatus())) {
				validateFileStatus(response.getFileSnapshotDto().getFileStatus());
				fileUrl = fileGenFactory.getFileGeneratorByFileExtn(response.getFileConfigSnapshotDto().getFileExtension())
						.generateFile(request, response);
			}
			return ResponseGenUtil.getSuccessResponse(fileUrl, fileId);
		} catch (FileManagementLibException e) {
			return ResponseGenUtil.getFailureResponse(fileUrl, fileId, e);
		}
	}

	@Override
	public FileGenListResponse generateFileByTimeRange(FileGenRequest request) {
		FileGenListResponse genResponse = new FileGenListResponse();
		List<FileGenResponse> responseList = new ArrayList<>();
		// Fetch all the fileIds for which we need to generate the file, i.e.
		// with the applicable time range provided in request.
		List<FileSnapshotDto> dtoList = snapshotFetcherService.getByUpdateTsRange(
				request.getUpdateTSRange().getStartTime(), request.getUpdateTSRange().getEndTime(),
				request.getFileConfigType());
		for (FileSnapshotDto dto : dtoList) {
			Long fileId = dto.getFileId();
			String fileUrl = null;
			try {
				FileSnapshotAndConfigResponse response = fileGenUtil.getSnapshotAndConfigByFileId(fileId);
				if (!isAlreadyComplete(response.getFileSnapshotDto().getFileStatus())) {
					fileUrl = fileGenFactory
							.getFileGeneratorByFileExtn(response.getFileConfigSnapshotDto().getFileExtension())
							.generateFile(request, response);
				}
				responseList.add(ResponseGenUtil.getSuccessResponse(fileUrl, fileId));
			} catch (FileManagementLibException e) {
				responseList.add(ResponseGenUtil.getFailureResponse(fileUrl, fileId, e));
				log.error("Error occured in generateFileByTimeRange for fileId: {}. Error: {} ", fileId, e);
			}
		}
		genResponse.setFileGenResponseList(responseList);
		return genResponse;
	}
	
	@Override
	public boolean isAlreadyComplete(FileStatus fileStatus) {
		return fileStatus == FileStatus.GEN_COMPLETE || fileStatus.getIntValue() >= FileStatus.UPLOAD_START.getIntValue();
	}
	
	@Override
	public void validateFileStatus(FileStatus fileStatus) {
		if (fileStatus != FileStatus.SNAPSHOT_COMPLETE && fileStatus != FileStatus.GEN_FAILED) {
			throw new InvalidFileStatusException("Cannot generate file. Invalid state for file gen:" + fileStatus);
		}

	}

}
