package com.freecharge.file.generator;

import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.request.ServiceRequest;
import com.freecharge.model.type.FileExtension;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileGenerator {
	public String generateFile(ServiceRequest request, FileSnapshotAndConfigResponse response);
	
	public FileExtension getFileExtension();
}
