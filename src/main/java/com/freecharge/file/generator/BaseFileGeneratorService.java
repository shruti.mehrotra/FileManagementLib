package com.freecharge.file.generator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.constants.FileRegexConstants;
import com.freecharge.dao.service.IDBRetrievalService;
import com.freecharge.exceptions.ExceptionErrorCode;
import com.freecharge.exceptions.FileGenerationException;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.exceptions.InvalidFileConfigException;
import com.freecharge.file.snapshot.IFileSnapshotUpdateService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.request.ServiceRequest;
import com.freecharge.model.type.FileStatus;
import com.freecharge.parser.FileTemplateParser;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Component
@Slf4j
public abstract class BaseFileGeneratorService implements IFileGenerator {

	@Autowired
	private FileTemplateParser templateParser;

	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotService;

	@Autowired
	@Qualifier("DBRetrievalServiceImpl")
	protected IDBRetrievalService retrievalService;
	
	public static final String TEMP_FOLDER = "/tmp/fml/";

	public abstract boolean createFile(FileTemplate fileTemplate, String fileUrl,
			FileSnapshotAndConfigResponse response);

	public String generateFile(ServiceRequest request, FileSnapshotAndConfigResponse response) {
		boolean genSuccess = false;
		String fileUrl = null;
		long fileId = response.getFileSnapshotDto().getFileId();
		snapshotService.updateFileStatus(FileStatus.GEN_START, fileId);
		log.info("Updated the file status to {}, and fileName to : {} for the fileId: {}", FileStatus.GEN_START,
				fileUrl, fileId);
		try {
			fileUrl = response.getFileSnapshotDto().getFileName();
			
			if (StringUtils.isEmpty(fileUrl)) {
				throw new FileManagementLibException("File URL found to be empty for the fileId: " + response.getFileSnapshotDto().getFileId());
			}
			
			FileTemplate fileTemplate = templateParser
					.getFileTemplateObjFromStr(response.getFileConfigSnapshotDto().getFileTemplate(), fileId, request);

			File filePathDir = new File(TEMP_FOLDER + fileUrl.substring(0, fileUrl.lastIndexOf(FileRegexConstants.FORWARD_SLASH)));

			try {
				filePathDir.mkdirs();
				File file = new File(TEMP_FOLDER + fileUrl);
				file.createNewFile();
			} catch (IOException e) {
				throw new InvalidFileConfigException("Unable to create file! fileName: " + fileUrl + " ,fileId: "
						+ fileId + " Error: " + e.getMessage(), ExceptionErrorCode.INVALID_FILE_REGEX_PATTERN);
			}
			boolean fileCreated = createFile(fileTemplate, TEMP_FOLDER + fileUrl, response);

			if (!fileCreated) {
				throw new FileGenerationException(
						"Unable to create file with url: " + fileUrl + " for the fileId: " + fileId);
			}

			log.info("File: {} generated for the fileId: {}", fileUrl, fileId);

			// Update file status in the snapshot to GEN
			snapshotService.updateFileStatus(FileStatus.GEN_COMPLETE, fileId);
			log.info("Updated the file status to {}, for the fileId: {}", FileStatus.GEN_COMPLETE, fileId);
			genSuccess = true;
		} catch (Exception e) {
			log.error("Unknown error occurred while generating file for the request: {} and config response: {}. Error: {}", request.toString(), response.toString(), e);
			throw e;
		} finally {
			if (!genSuccess) {
				snapshotService.updateFileStatus(FileStatus.GEN_FAILED, fileId);
				log.info("Updated the file status to {}, and fileName to : {} for the fileId: {}", FileStatus.GEN_FAILED,
						fileUrl, fileId);
			}
		}
		return fileUrl;
	}

}
