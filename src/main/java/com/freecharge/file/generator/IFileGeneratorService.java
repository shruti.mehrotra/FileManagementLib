package com.freecharge.file.generator;

import com.freecharge.file.IFileStepValidator;
import com.freecharge.model.request.FileGenRequest;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.response.FileGenListResponse;
import com.freecharge.model.response.FileGenResponse;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileGeneratorService extends IFileStepValidator {
	FileGenListResponse generateFileByTimeRange(FileGenRequest request);

	FileGenResponse generateFileBySnapshotRequest(FileSnapshotRequest request);
}
