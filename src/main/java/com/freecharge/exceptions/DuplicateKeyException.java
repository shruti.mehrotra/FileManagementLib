package com.freecharge.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class DuplicateKeyException extends FileManagementLibException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7973793474005796995L;

	public DuplicateKeyException() {
	}

	public DuplicateKeyException(String message) {
		super(message);
	}

	public DuplicateKeyException(String message, Throwable e) {
		super(message, e);
	}

	public DuplicateKeyException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.DUPLICATE_KEY_ERROR);
	}

}
