package com.freecharge.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class FileConfigNotFoundException extends FileManagementLibException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7698215804530088826L;

	public FileConfigNotFoundException() {
	}

	public FileConfigNotFoundException(String message) {
		super(message);
	}

	public FileConfigNotFoundException(String message, Throwable e) {
		super(message, e);
	}

	public FileConfigNotFoundException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.FILE_CONFIG_NOT_FOUND);
	}

}
