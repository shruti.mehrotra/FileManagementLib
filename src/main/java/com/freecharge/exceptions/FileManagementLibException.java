package com.freecharge.exceptions;

import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 * 
 */
@NoArgsConstructor
public class FileManagementLibException extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5590708342883757683L;
	private ExceptionErrorCode errorCode;
	private Class<? extends FileManagementLibException> exceptionCause;

	public FileManagementLibException withErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
		return this;

	}

	public FileManagementLibException(Throwable e) {
		super(e);
	}

	public FileManagementLibException(String message) {
		super(message);
	}

	public FileManagementLibException(String message, Throwable e) {
		super(message, e);
	}

	public ExceptionErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Class<?> getExceptionCause() {
		return exceptionCause;
	}

	public void setExceptionCause(
			Class<? extends FileManagementLibException> exceptionCause) {
		this.exceptionCause = exceptionCause;
	}

	{
		this.setExceptionCause(this.getClass());
	}

}
