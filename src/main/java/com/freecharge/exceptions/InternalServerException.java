package com.freecharge.exceptions;

import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 * 
 */
@NoArgsConstructor
public class InternalServerException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5096979937293707296L;
	
	
	private ExceptionErrorCode errorCode;
	private Class<? extends InternalServerException> exceptionCause;

	public InternalServerException withErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
		return this;

	}

	public InternalServerException(Throwable e) {
		super(e);
	}

	public InternalServerException(String message) {
		super(message);
	}

	public InternalServerException(String message, Throwable e) {
		super(message, e);
	}

	public ExceptionErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Class<?> getExceptionCause() {
		return exceptionCause;
	}

	public void setExceptionCause(
			Class<? extends InternalServerException> exceptionCause) {
		this.exceptionCause = exceptionCause;
	}

	{
		this.setExceptionCause(this.getClass());
	}

}
