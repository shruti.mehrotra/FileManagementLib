package com.freecharge.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class QueryExecutionException extends FileManagementLibException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2326752708182655644L;

	public QueryExecutionException() {
	}

	public QueryExecutionException(String message) {
		super(message);
	}

	public QueryExecutionException(String message, Throwable e) {
		super(message, e);
	}

	public QueryExecutionException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.INVALID_QUERY);
	}

}
