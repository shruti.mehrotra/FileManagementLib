package com.freecharge.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class DatabaseConnectionException extends FileManagementLibException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -2485551118993493313L;

	public DatabaseConnectionException() {
	}

	public DatabaseConnectionException(String message) {
		super(message);
	}

	public DatabaseConnectionException(String message, Throwable e) {
		super(message, e);
	}

	public DatabaseConnectionException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.DB_CONNECT_ERROR);
	}

}
