package com.freecharge.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class ValidationException extends FileManagementLibException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -6848932585259780542L;

	public ValidationException(ExceptionErrorCode errorCode) {
		setErrorCode(errorCode);
	}

	public ValidationException(String message, ExceptionErrorCode errorCode) {
		super(message);
		setErrorCode(errorCode);
	}

	public ValidationException(String message, ExceptionErrorCode errorCode, Throwable e) {
		super(message, e);
		setErrorCode(errorCode);
	}

	public ValidationException(ExceptionErrorCode errorCode, Throwable e) {
		super(e);
		setErrorCode(errorCode);
	}

}
