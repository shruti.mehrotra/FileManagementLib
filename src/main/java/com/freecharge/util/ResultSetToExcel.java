package com.freecharge.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.lang.exception.NestableException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.contrib.HSSFCellUtil;

import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.model.dto.template.ValueType;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@NoArgsConstructor
@Slf4j
public class ResultSetToExcel {
	@Setter
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFFont boldFont;
	private HSSFDataFormat format;
	private ResultSet resultSet;
	private FormatType[] formatTypes;
	private DBQueryDto dbQueryDto;

	public void initialize(ResultSet resultSet, DBQueryDto dbQueryDto, 
						FormatType[] formatTypes, String sheetName) {
		if (workbook == null) {
			workbook = new HSSFWorkbook();
		}
		this.resultSet = resultSet;
		sheet = workbook.createSheet(sheetName);
		boldFont = workbook.createFont();
		boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		format = workbook.createDataFormat();
		this.formatTypes = formatTypes;
		this.dbQueryDto = dbQueryDto;
	}

	public void initialize(ResultSet resultSet, DBQueryDto dbQueryDto, String sheetName) {
		initialize(resultSet, dbQueryDto, null, sheetName);
	}

	private FormatType getFormatType(Class _class) {
		if (_class == Integer.class || _class == Long.class) {
			return FormatType.INTEGER;
		} else if (_class == Float.class || _class == Double.class) {
			return FormatType.FLOAT;
		}/* else if (_class == Timestamp.class || _class == java.sql.Date.class) {
			return FormatType.DATE;
		}*/ else {
			return FormatType.TEXT;
		}
	}

	public void generate(OutputStream outputStream) throws Exception {
		
		int currentRow = 0;
		HSSFRow row = null;
		if (dbQueryDto.getBatchHeader() != null && dbQueryDto.getBatchHeader().getColumns() != null) {
			row = sheet.createRow(currentRow++);
			for (int i = 0; i < dbQueryDto.getBatchHeader().getColumns().length; i++) {
				String value = dbQueryDto.getBatchHeader().getColumns()[i].getName();
				writeCell(row, i, value, FormatType.TEXT);
			}
		}
		try {
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			if (formatTypes != null && formatTypes.length != resultSetMetaData.getColumnCount()) {
				throw new IllegalStateException(
						"Number of types is not identical to number of resultset columns. " + "Number of types: "
								+ formatTypes.length + ". Number of columns: " + resultSetMetaData.getColumnCount());
			}
//			row = sheet.createRow(currentRow);
			int numCols = resultSetMetaData.getColumnCount();
			boolean isAutoDecideFormatTypes;
			if (isAutoDecideFormatTypes = (formatTypes == null)) {
				formatTypes = new FormatType[numCols];
			}
			for (int i = 0; i < numCols; i++) {
//				String title = resultSetMetaData.getColumnName(i + 1);
//				writeCell(row, i, title, FormatType.TEXT, boldFont);
				if (isAutoDecideFormatTypes) {
					Class _class = Class.forName(resultSetMetaData.getColumnClassName(i + 1));
					formatTypes[i] = getFormatType(_class);
				}
			}
			// Write report rows
			while (resultSet.next()) {
				row = sheet.createRow(currentRow++);
				for (int i = 0; i < numCols; i++) {
					int columnNum = i;
					Object value = null;
					if (dbQueryDto.getAllColumns()[columnNum].getValueType() == ValueType.STATIC) {
						value = dbQueryDto.getAllColumns()[i].getName();
						writeCell(row, columnNum, value, formatTypes[i]);
						columnNum++;
					}
					try {
						value = resultSet.getObject(i + 1);
					} catch (SQLException e) {
						log.error("Error fetching object from result set for XLS: ", e);
					}
					writeCell(row, columnNum, value, formatTypes[i]);
				}
			}
			// Autosize columns
			for (int i = 0; i < numCols; i++) {
				sheet.autoSizeColumn((short) i);
			}
			
			row = sheet.createRow(currentRow);
			if (dbQueryDto.getBatchFooter() != null && dbQueryDto.getBatchFooter().getColumns() != null) {
				for (int i = 0; i < dbQueryDto.getBatchFooter().getColumns().length; i++) {
					String value = dbQueryDto.getBatchFooter().getColumns()[i].getName();
					writeCell(row, i, value, FormatType.TEXT);
				}
			}
			
			workbook.write(outputStream);
		} finally {
			outputStream.close();
		}
	}

	public void generate(File file) throws Exception {
		generate(new FileOutputStream(file));
	}

	private void writeCell(HSSFRow row, int col, Object value, FormatType formatType) throws NestableException {
		writeCell(row, col, value, formatType, null, null);
	}

	private void writeCell(HSSFRow row, int col, Object value, FormatType formatType, HSSFFont font)
			throws NestableException {
		writeCell(row, col, value, formatType, null, font);
	}

	private void writeCell(HSSFRow row, int col, Object value, FormatType formatType, Short bgColor, HSSFFont font)
			throws NestableException {
		HSSFCell cell = HSSFCellUtil.createCell(row, col, null);
		if (value == null) {
			return;
		}
		if (font != null) {
			HSSFCellStyle style = workbook.createCellStyle();
			style.setFont(font);
			cell.setCellStyle(style);
		}
		switch (formatType) {
		case TEXT:
			cell.setCellValue(value.toString());
			break;
		case INTEGER:
			cell.setCellValue(((Number) value).intValue());
			break;
		case FLOAT:
			cell.setCellValue(((Number) value).doubleValue());
			break;
		case DATE:
			cell.setCellValue((Timestamp) value);
			break;
		case MONEY:
			cell.setCellValue(((Number) value).intValue());
			break;
		case PERCENTAGE:
			cell.setCellValue(((Number) value).doubleValue());
		}
	}

	public enum FormatType {
		TEXT, INTEGER, FLOAT, DATE, MONEY, PERCENTAGE
	}
}
