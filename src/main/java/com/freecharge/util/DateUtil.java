package com.freecharge.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class DateUtil {
	
	public static final String YYYYDDMM_HYPHENED_HHMMSS_COLONED = "yyyy-MM-dd HH:mm:ss";
	
	public static Date subtractDay(Date date, int subtractIndex) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.DAY_OF_MONTH, -subtractIndex);
	    return cal.getTime();
	}
	
	public static Date currentDay() {
	    Calendar cal = Calendar.getInstance();
	    return cal.getTime();
	}
	
	public static String formatDate(Date date, String format) {
		if (StringUtils.isEmpty(format)) {
			format = YYYYDDMM_HYPHENED_HHMMSS_COLONED;
		}
		SimpleDateFormat dt = new SimpleDateFormat(format);
		return dt.format(date);
	}
}
