package com.freecharge.util;

import java.util.UUID;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */
public class GuidGenerator {
	public static String getTimestampedGuid() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	public static long getTimestampedGtid() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();
		UUID uuid = timeBasedGenerator.generate();
		return uuid.timestamp();
	}
	public static void main(String[] args) {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();

        //Generate time based UUID
        UUID firstUUID = timeBasedGenerator.generate();
        System.out.printf("1. First UUID is : %s ", firstUUID.toString());
        System.out.printf("\n2. Timestamp of UUID is : %d ", firstUUID.timestamp());

        UUID secondUUID = timeBasedGenerator.generate();
        System.out.printf("\n3. Second UUID is :%s ", secondUUID.toString());
        System.out.printf("\n4. Timestamp of UUID is : %d ", secondUUID.timestamp());

        //Generate custom UUID from network interface
        timeBasedGenerator = Generators.timeBasedGenerator(EthernetAddress.fromInterface());
        UUID customUUID = timeBasedGenerator.generate();
        UUID anotherCustomUUID = timeBasedGenerator.generate();

        System.out.printf("\n5. Custom UUID is :%s ", customUUID.toString());
        System.out.printf("\n6. Another custom UUID : %s ", anotherCustomUUID.toString());
	}
}
