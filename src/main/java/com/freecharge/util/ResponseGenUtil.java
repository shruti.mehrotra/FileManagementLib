package com.freecharge.util;

import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.response.ServiceResponse;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class ResponseGenUtil {
	public static ServiceResponse getSuccessServiceResponse(String message) {
		ServiceResponse response = new ServiceResponse();
		response.setSuccess(true);
		response.setMessage(message);
		return response;
	}

	public static ServiceResponse getFailureServiceResponse(FileManagementLibException e) {
		ServiceResponse response = new ServiceResponse();
		response.setSuccess(false);
		response.setMessage(e.getMessage());
		response.setErrorCode(e.getErrorCode());
		return response;
	}

	public static FileGenResponse getFailureResponse(String fileUrl, Long fileId, FileManagementLibException e) {
		FileGenResponse response = getSuccessResponse(fileUrl, fileId);
		response.setMessage(e.getMessage());
		response.setSuccess(false);
		response.setErrorCode(e.getErrorCode());
		return response;
	}

	public static FileGenResponse getSuccessResponse(String fileUrl, Long fileId) {
		FileGenResponse response = new FileGenResponse();
		if (fileId != null) {
			response.setFileId(fileId.toString());
		}
		response.setFileUrl(fileUrl);
		response.setSuccess(true);
		response.setMessage("Request processed successfully!");
		return response;

	}
}
