package com.freecharge.util;

import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import com.freecharge.constants.FileRegexConstants;
import com.freecharge.dao.dto.DBQueryResponse;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class AppUtil {
	public static String taggenateString(String subStr) {
		StringBuilder taggedString = new StringBuilder();
		taggedString.append(FileRegexConstants.OPEN_TAG).append(subStr).append(FileRegexConstants.CLOSE_TAG);

		return taggedString.toString();
	}
	
	public static String quoteString(String input) {
		StringBuilder output = new StringBuilder();
		if (!input.startsWith("'")) {
			output.append("'");
		}
		output.append(input);
		
		if (!input.endsWith("'")) {
			output.append("'");
		}
		return output.toString();
	}

	public static String[] getStringsBetweenTags(String input) {
		String[] substringsBetween = StringUtils.substringsBetween(input, FileRegexConstants.OPEN_TAG,
				FileRegexConstants.CLOSE_TAG);
		return substringsBetween;
	}
	
	public static void closeDBConnections(DBQueryResponse dbResponse) {
		if (dbResponse != null) {
			try {
				if (dbResponse.getCon() != null) {
					dbResponse.getCon().close();
				}
			} catch (SQLException se1) {
				/* can't do anything */
			}
			try {
				if (dbResponse.getStmt() != null) {
					dbResponse.getStmt().close();
				}
			} catch (SQLException se1) {
				/* can't do anything */
			}
			try {
				if (dbResponse.getRs() != null) {
					dbResponse.getRs().close();
				}
			} catch (SQLException se) {
				/* can't do anything */
			}
		}
	}
}
