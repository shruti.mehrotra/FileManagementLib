package com.freecharge.util;

import java.util.Arrays;

import com.freecharge.model.dto.DataStats;
import com.freecharge.model.dto.DynamicRoutingData;
import com.freecharge.model.dto.TrendStats;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class TrendIdentificationAlgorithm {
	
	public static final double STANDARD_AVG_PERCENTAGE = 20;
	
	public static boolean isUptrendingData(double[] input, int inputLen) {
		TrendStats trendStats = findTrendDataForConsecutiveElements(input, inputLen);
		
		return isUptrendingData(trendStats);
		
	}
	
	public static boolean isUptrendingData(TrendStats trendStats) {
		boolean isPositiveNetPercentage = trendStats.getNetPercentage() >= 0;
		boolean isStandardAverage = Math.floor(trendStats.getAveragePercentage()) >= STANDARD_AVG_PERCENTAGE;
		
		return isPositiveNetPercentage && isStandardAverage;
		
	}
	
	public static DynamicRoutingData getDynamicRoutingData(double[] input, int inputLen) {
		DynamicRoutingData routingData = new DynamicRoutingData();
		
		DataStats dataStats = getDataStats(input, inputLen);
		double[] normalPoints = new double[inputLen];
		double average = dataStats.getAverage();
		double stdDev = dataStats.getStdev();
		int normalIndex = 0;
		for (int i = 0; i < inputLen; i++) {
			if (input[i] <= average + stdDev && input[i] >= average - stdDev) {
				normalPoints[normalIndex++] = input[i];
			}
		}
		TrendStats trendStats = findTrendDataForConsecutiveElements(normalPoints, normalIndex);
		routingData.setTrendStats(trendStats);
		routingData.setUptrending(isUptrendingData(trendStats));
		routingData.setStandardPercentArr(Arrays.copyOfRange(normalPoints, 0, normalIndex));
		
		return routingData;
		
	}
	
	public static double getPercentageFluctuation(double newPercentage, double originalPercentage) {
		return (newPercentage-originalPercentage)/originalPercentage;
	}

	public static TrendStats findTrendDataForConsecutiveElements(double[] input, int inputLen) {
		double peakValue = input[0];
		double netPercentage = 0;
		double averagePercentage = 0;
		int peakIndex = 0;
		for (int i = 0; i < inputLen - 1; ++i) {
			double newPercentage = input[i + 1];
			double originalPercentage = input[i];
			netPercentage += getPercentageFluctuation(newPercentage, originalPercentage);
			if (input[i] > peakValue) {
				peakValue = input[i];
				peakIndex = i;
			}
			averagePercentage+= originalPercentage;
		}
		averagePercentage/=inputLen;
		return new TrendStats(peakValue, peakIndex, averagePercentage, netPercentage);
	}

	public static DataStats getDataStats(double[] input, int inputLen) {

		Statistics statistics = new Statistics(input, inputLen);
		double stdev = statistics.getStdDev();

		double mean = statistics.getMean();

		double coefficientOfVariation = (stdev / mean) * 100;

		double upperbound = mean + stdev;
		double lowerbound = mean - stdev;
		
		if (lowerbound < 0) {
			lowerbound = 0;
		}
		return new DataStats(stdev, mean, coefficientOfVariation, upperbound, lowerbound);
	}
	
	public static void main(String[] args) {
//		double input[] = { 65, 28, 70, 25, 45, 5, 5, 50, 30, 65 };
//		double input[] = { 65, 28, 70, 25, 45, 5, 5, 1, 1, 1 };
		
		double input[] = { 65, 40, 70, 1, 1, 10, 5, 1, 1, 1 };
		double[] pointsMoreThanAveragePlusStdDev = new double[input.length];
		double[] pointsLessThanAverageMinusStdDev = new double[input.length];
		double[] normalPoints = new double[input.length];

		DataStats dataStats = getDataStats(input, input.length);

		double average = dataStats.getAverage();
		double stdDev = dataStats.getStdev();
		int moreIndex = 0, lessIndex = 0, normalIndex = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] > average + stdDev) {
				pointsMoreThanAveragePlusStdDev[moreIndex++] = input[i];
			} else if (input[i] < average - stdDev) {
				pointsLessThanAverageMinusStdDev[lessIndex++] = input[i];
			} else {
				normalPoints[normalIndex++] = input[i];
			}
		}
		System.out.println("Actual data stats: " + dataStats.toString());

		if (moreIndex != 0){
			System.out.println("MoreIndex:" + moreIndex);
			DataStats dataStatsOfLargerNos = getDataStats(pointsMoreThanAveragePlusStdDev, moreIndex);
			System.out.println("Data stats of larger numbers: " + dataStatsOfLargerNos.toString());
		}
		
		if (lessIndex != 0) {
			System.out.println("LessIndex:" + lessIndex);
			DataStats dataStatsOfSmallerNos = getDataStats(pointsLessThanAverageMinusStdDev, lessIndex);
			System.out.println("Data stats of smaller numbers: " + dataStatsOfSmallerNos.toString());
		}

		System.out.println("NormalIndex:" + normalIndex);
		DataStats dataStatsOfNormalNos = getDataStats(normalPoints, normalIndex);
		System.out.println("Data stats of Normal numbers: " + dataStatsOfNormalNos.toString());

		System.out.println("Is uptrending:" + isUptrendingData(normalPoints, normalIndex));
		
		System.out.println("Dynamic Routing stats: " + getDynamicRoutingData(input, input.length));
	}

}
