package com.freecharge.util;

import org.apache.commons.codec.digest.DigestUtils;

import com.freecharge.model.request.FileSnapshotRequest;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class HashUtil {

	public static final String SALT = "fmlFilter";

	public static String getSHA512Hash(String strToHash) {
		return DigestUtils.sha512Hex(strToHash + SALT);
	}

	public static String getHashByFileManagementRequest(FileSnapshotRequest request) {
		StringBuilder strToHash = new StringBuilder();
		strToHash.append(request.getFileConfigType().name());
		strToHash.append(request.getFilterForSnapshotRecords().toString());
		return HashUtil.getSHA512Hash(strToHash != null ? strToHash.toString() : null);
	}
	
	
}
