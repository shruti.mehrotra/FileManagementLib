package com.freecharge.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.constants.DBConstants;
import com.freecharge.constants.FileRegexConstants;
import com.freecharge.dao.dto.KeyValDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.dao.dto.WhereClause;
import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.file.snapshot.IFileSnapshotFetcherService;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.FileObject;
import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.dto.template.Column;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileExtension;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Component
@Slf4j
public class FileGenUtil {
	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;

	public FileSnapshotAndConfigResponse getSnapshotAndConfigByFileId(long fileId) {
		return snapshotFetcherService.getFileStateAndConfigByFileId(fileId);
	}

	public List<FileSnapshotDto> getFileManagementDtoByFileType(FileConfigType fileConfigType) {
		return snapshotFetcherService.getFileManagementDtoListByConfigType(fileConfigType);
	}
	
	public String getCompleteFileName(FileObject fileObj) {
		StringBuilder fileUrl = new StringBuilder();
		fileUrl.append(fileObj.getFilePath());
		fileUrl.append(fileObj.getFileName());
		return fileUrl.toString();
	}

	public String getValidFilePath(String filePath) {
		if (!(filePath.endsWith(FileRegexConstants.FORWARD_SLASH)
				|| filePath.endsWith(FileRegexConstants.BACKWARD_SLASH))) {
			filePath += FileRegexConstants.FORWARD_SLASH;
		}

		return filePath;
	}

	public FileObject getFileObjectForRegexDto(FileConfigSnapshotDto response, List<KeyValDto> dtoList) {
		response = resolveRegexInDto(response, dtoList);
		String fileName = response.getFileNameRegex();
		String filePath = getValidFilePath(response.getFilePathRegex());
		return new FileObject(filePath, fileName);
	}

	public static FileConfigSnapshotDto resolveRegexInDto(FileConfigSnapshotDto response,
			List<KeyValDto> dtoList) {

		FileConfigSnapshotDto finalDto = response;

		finalDto.setFilePathRegex(resolveRegexInString(response.getFilePathRegex(), response, dtoList));
		StringBuilder absFileName = new StringBuilder(
				resolveRegexInString(response.getFileNameRegex(), response, dtoList));
		if (!absFileName.toString().endsWith("." + response.getFileExtension().getExtension())) {
			absFileName.append(".").append(response.getFileExtension().getExtension());
		}
		finalDto.setFileNameRegex(absFileName.toString());

		return finalDto;
	}

	public static String resolveRegexInString(String input, FileConfigSnapshotDto response,
			List<KeyValDto> dtoList) {
		List<String> allowedRegexList = getAllowedRegexList();
		Map<String, Object> allowedRegexMap = getAllowedRegexMap(response);
		String[] substringsBetween = AppUtil.getStringsBetweenTags(input);
		
		if (substringsBetween != null) {
			
			for (String subStr : substringsBetween) {
				if (allowedRegexList.contains(subStr) || startsWithAny(subStr, allowedRegexList)) {
					if (allowedRegexMap.containsKey(subStr)) {
						input = StringUtils.replace(input, AppUtil.taggenateString(subStr), allowedRegexMap.get(subStr).toString());
					} else if (dtoList != null && !dtoList.isEmpty()) {
						for (KeyValDto dto : dtoList) {
							if (subStr.contains(dto.getKey())) {
								input = StringUtils.replace(input, AppUtil.taggenateString(subStr), dto.getValue());
							}
						}
					} else {
						log.error(
								"Could not resolve one of taggenated string component: {} for the input regex: {} "
										+ "as was neither configured in request nor in allowedRegexMap!",
										subStr, input);
					}
				} else {
					log.error("Could not resolve one of taggenated string component: {} for the input regex: {}!", subStr,
							input);
				}
			}
		}

		return input;
	}

	private static boolean startsWithAny(String subStr, List<String> allowedRegexList) {
		for (String allowedRegex: allowedRegexList) {
			if (subStr.startsWith(allowedRegex)) {
				return true;
			}
		}
		return false;
	}

	public static Map<String, Object> getDateRegexMap() {
		List<String> datePatternList = getDateFormatList();

		Map<String, Object> dateRegexMap = new HashMap<>();
		Date date = new Date();

		for (String datePattern : datePatternList) {
			SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			dateRegexMap.put(datePattern, formatter.format(date));
		}

		return dateRegexMap;
	}

	public static List<String> getAllowedRegexList() {
		List<String> dateFormatList = getDateFormatList();
		List<String> allowedRegexList = new ArrayList<>(dateFormatList);
		allowedRegexList.add(DBConstants.FILE_TYPE);
		allowedRegexList.add(FileRegexConstants.REQUEST);
		return allowedRegexList;
	}

	public static Map<String, Object> getAllowedRegexMap(FileConfigSnapshotDto response) {
		Map<String, Object> allowedRegexMap = getDateRegexMap();
		allowedRegexMap.put(DBConstants.FILE_TYPE, response.getFileConfigType().name());
		return allowedRegexMap;
	}

	public static List<String> getDateFormatList() {
		List<String> dateFormatList = Arrays.asList("ddMMyyyy", "MM/dd/yyyy", "dd-M-yyyy hh:mm:ss", "dd MMMM yyyy",
				"dd MMMM yyyy zzzz", "E, dd MMM yyyy HH:mm:ss z", "ddMMyyyyhhmmss", "yyyyMMdd");
		return dateFormatList;
	}

	public static List<KeyValWithDbOperatorDto> getFileIdDtoList(Long fileId) {
		List<KeyValWithDbOperatorDto> dtoList = new ArrayList<>();
		dtoList.add(new KeyValWithDbOperatorDto(DBConstants.FILE_ID, fileId.toString(), DBOperator.EQUALS, null));
		return dtoList;
	}

	public static WhereClause getWhereClauseByColumnMap(List<KeyValWithDbOperatorDto> dtoList) {
		return new WhereClause(new KeyValWithDbOperatorList(dtoList));
	}

	public static String[] toStringArray(Column[] columns) {
		if (columns != null && columns.length > 0) {
			String[] columnNames = new String[columns.length];
			for (int i = 0; i < columns.length; i++) {
				columnNames[i] = columns[i].getName();
			}
			return columnNames;
		}
		return null;
	}

	public static void main(String[] args) {
		FileConfigSnapshotDto fileConfigDto = new FileConfigSnapshotDto();
		fileConfigDto.setFilePathRegex("conf/<FILE_TYPE>/<ddMMyyyy>/<FILE_ID>");
		fileConfigDto.setFileNameRegex("<MM/dd/yyyy>refund");
		fileConfigDto.setFileExtension(FileExtension.CSV);
		fileConfigDto.setFileConfigType(FileConfigType.REFUND_HDFC);
		
		System.out.println(resolveRegexInDto(fileConfigDto, null));

	}

}
