package com.freecharge.model.response;

import com.freecharge.exceptions.ExceptionErrorCode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Getter
@Setter
@ToString
public class ServiceResponse {
	private boolean isSuccess;
	private String message;
	private ExceptionErrorCode errorCode;
}
