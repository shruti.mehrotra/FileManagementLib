package com.freecharge.model.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Setter
@Getter
@ToString
public class FileGenListResponse {
	private List<FileGenResponse> fileGenResponseList;
}
