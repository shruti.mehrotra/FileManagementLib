package com.freecharge.model.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Setter
@Getter
@ToString
public class FileGenResponse extends ServiceResponse {
	private String fileId;
	private String fileUrl;
}
