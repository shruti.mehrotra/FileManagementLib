package com.freecharge.model.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
@Component
public class FileUploadResponse {
	Map<String, ServiceResponse> responseMap;
	
	public FileUploadResponse() {
		this.responseMap = new HashMap<>();
	}
}