package com.freecharge.model.dto;

import lombok.ToString;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class DynamicRoutingData {
	private boolean isUptrending;
	private double[] standardPercentArr;
	private TrendStats trendStats;
}
