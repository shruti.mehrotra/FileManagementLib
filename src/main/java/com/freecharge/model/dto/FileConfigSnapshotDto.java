package com.freecharge.model.dto;

import com.freecharge.dao.entity.FileConfigEntity;
import com.freecharge.model.type.DestinationType;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileExtension;
import com.freecharge.model.type.FileFlowType;
import com.freecharge.model.type.NotificationType;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileConfigSnapshotDto {
	private long fileConfigId;
	private FileConfigType fileConfigType;
	private String fileTemplate;
	private String filePathRegex;
	private String fileNameRegex;
	private FileExtension fileExtension;
	private FileFlowType fileFlowType;
	private DestinationType destinationType;
	private String destinationConfigFN;
	private S3Config s3Config;
	private NotificationType notificationType;
	private String notificationConfig;
	private String createTS;
	private String updateTS;

	public FileConfigEntity toEntity() {
		FileConfigEntity entity = new FileConfigEntity();
		entity.setFileConfigId(this.fileConfigId);
		entity.setFileConfigType(this.fileConfigType.toString());
		entity.setFileTemplate(this.fileTemplate);
		entity.setFilePathRegex(this.filePathRegex);
		entity.setFileNameRegex(this.fileNameRegex);
		entity.setFileExtension(this.fileExtension.name());
		entity.setFileFlowType(this.fileFlowType.toString());
		entity.setDestinationType(this.destinationType.toString());
		entity.setDestinationConfigFN(this.destinationConfigFN);
		entity.setNotificationType(this.notificationType.name());
		entity.setNotificationConfig(this.notificationConfig);
		entity.setCreationTS(this.createTS);
		entity.setUpdateTS(this.updateTS);
		return entity;
	}
}
