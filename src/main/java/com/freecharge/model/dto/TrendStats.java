package com.freecharge.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@AllArgsConstructor
public class TrendStats {
	private double peak;
	private int peakIndex;
	private double averagePercentage;
	private double netPercentage;
}
