package com.freecharge.model.dto;

import java.util.Date;

import com.freecharge.util.DateUtil;

public class TimeRange {

	private Date startTime;
	
	private Date endTime;
	
	public TimeRange() {}
	public TimeRange(Date startDate, Date endDate) {
		super();
		this.startTime = startDate;
		this.endTime = endDate;
	}
	
	public String getStartTime() {
		return DateUtil.formatDate(startTime, "yyyy-MM-dd HH:mm:ss");
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return DateUtil.formatDate(endTime, "yyyy-MM-dd HH:mm:ss");
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "TimeRange [startDate=" + getStartTime() + ", endDate=" + getEndTime() + "]";
	}
	
}
