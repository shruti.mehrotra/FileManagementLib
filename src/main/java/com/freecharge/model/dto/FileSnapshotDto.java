package com.freecharge.model.dto;

import com.freecharge.dao.entity.FileManagementEntity;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileStatus;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileSnapshotDto {
	private long fileId;
	private String fileName;
	private FileConfigType fileConfigType;
	private String creationTS;
	private String updateTS;
	private FileStatus fileStatus;
	private long fileConfigId;
	private String recFilterHash;
	private long numOfRecords;

	public FileManagementEntity toEntity() {
		FileManagementEntity snapshotEntity = new FileManagementEntity();
		snapshotEntity.setFileId(this.fileId);
		snapshotEntity.setFileName(this.fileName);
		snapshotEntity.setFileStatus(this.fileStatus.toString());
		snapshotEntity.setCreationTS(this.creationTS);
		snapshotEntity.setUpdateTS(this.updateTS);
		snapshotEntity.setFileConfigId(this.fileConfigId);
		snapshotEntity.setFileConfigType(this.fileConfigType.name());
		snapshotEntity.setRecFilterHash(this.recFilterHash);
		snapshotEntity.setNumOfRecords(this.numOfRecords);
		return snapshotEntity;
	}
}
