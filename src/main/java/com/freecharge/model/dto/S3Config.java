package com.freecharge.model.dto;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import lombok.Data;

@Data
public class S3Config {
//	@Value("${fml.aws.bucket.name}")
	String bucketName;

//	@Value("${fml.aws.shared.access.key}")
	String s3SharedAccessId;

//	@Value("${fml.aws.shared.secret.key}")
	String s3SharedSecretKey;

//	@Value("${fml.aws.link.expirytime}")
	long expiryTime;

//	@Value("${fml.aws.region}")
	String region;
}
