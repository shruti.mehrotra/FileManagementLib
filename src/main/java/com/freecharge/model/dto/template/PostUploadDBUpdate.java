package com.freecharge.model.dto.template;

import com.freecharge.dao.dto.OnClause;
import com.freecharge.dao.dto.SetClause;
import com.freecharge.dao.dto.TableNameClause;
import com.freecharge.dao.dto.WhereClause;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class PostUploadDBUpdate {
	private TableNameClause tableNameClause;
	private WhereClause whereClause;
	private SetClause setClause;
	private OnClause onClause;

}
