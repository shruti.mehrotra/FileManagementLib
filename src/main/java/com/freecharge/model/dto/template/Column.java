package com.freecharge.model.dto.template;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class Column {
	@JsonProperty(required = true)
	private String id;
	@JsonProperty(required = true)
	private String name;
	private ValueType valueType = ValueType.DYNAMIC;
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
