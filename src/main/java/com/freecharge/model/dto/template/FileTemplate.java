package com.freecharge.model.dto.template;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.freecharge.config.ApplicationContextProvider;
import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.file.IFileStatusManagementValidator;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileTemplate {

	@JsonProperty(required=true)
	private String dbName;
	
	private Header fileHeader = new Header(ColumnType.HEADER.FILE);
	
	@NotNull
	@NotEmpty
	private Batch[] batches;
	
	private char fileDelimiter;
	
	private IFileStatusManagementValidator validatorClass;
	
	private PostUploadDBUpdate postUploadDBUpdate;
	
	public String[] dynamicColumnToStringArray(Column[] colArr) {
		if (colArr != null && colArr.length > 0) {
			String[] colStrArray = new String[colArr.length];
			
			for (int i = 0; i < colStrArray.length; i++) {
				if (colArr[i].getValueType() == ValueType.DYNAMIC) {
					colStrArray[i] = colArr[i].toString();
				}
			}
			
			return colStrArray;
		}
		return null;
	}
	
	public DBQueryDto[] toDBRetrievalDto() throws IOException {
		DBQueryDto[] dtoArray = new DBQueryDto[batches.length];
		for (int i = 0; i < this.batches.length; i++) {
			if (batches[i].getBody() != null) {
				DBQueryDto dto = new DBQueryDto();
				DBConfig dbConfig = getDBConfigByDbName(this.dbName);
				dto.setDbName(dbConfig.getDbName());
				dto.setDbUrl(dbConfig.getDbUrl());
				dto.setUserName(dbConfig.getUserName());
				dto.setPassword(dbConfig.getPassword());
				dto.setTableNameClause(batches[i].getBody().getTableNameClause());
				dto.setWhereClause(batches[i].getBody().getWhereClauseWithStaticVal());
				dto.setOnClause(batches[i].getBody().getOnClause());
				dto.setBatchHeader(batches[i].getBatchHeader());
				dto.setBatchFooter(batches[i].getBatchFooter());
				dto.setColumnsToSelect(dynamicColumnToStringArray(batches[i].getBody().getColumns()));
				dto.setBatchSheetName(batches[i].getBatchName());
				dto.setAllColumns(batches[i].getBody().getColumns());
				dtoArray[i] = dto;
			}
		}
		return dtoArray;
	}
	
	public DBConfig getDBConfigByDbName(String dbName) throws IOException {
		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		DBConfig bean = (DBConfig) ctx.getBean(dbName);
		bean.setDbName(dbName);
		/*DBConfig dbConfig = new DBConfig();
		dbConfig.setDbName(dbName);
		dbConfig.setHost(prop.getProperty("host"));
		dbConfig.setUserName(prop.getProperty("username"));
		dbConfig.setPassword(prop.getProperty("password"));*/
		return bean;
	}
}
