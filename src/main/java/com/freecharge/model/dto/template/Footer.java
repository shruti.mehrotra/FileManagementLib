package com.freecharge.model.dto.template;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.freecharge.model.dto.template.ColumnType.FOOTER;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class Footer {
	@JsonProperty(required = true)
	private Column[] columns;
	private FOOTER type;

	public Footer(FOOTER footer) {
		this.type = footer;
	}
}
