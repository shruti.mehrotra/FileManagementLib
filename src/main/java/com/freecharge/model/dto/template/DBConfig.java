package com.freecharge.model.dto.template;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class DBConfig {
	private String dbName;
	private String dbUrl;
	private String userName;
	private String password;
}
