package com.freecharge.model.dto.template;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum ColumnType {
	BODY; 
	enum FOOTER{
		COUNT,
		SUM;
	}
	enum HEADER{
		BATCH, 
		FILE;
	}

}
