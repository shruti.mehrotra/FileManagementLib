package com.freecharge.model.dto.template;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum ValueType {
	STATIC, DYNAMIC
}
