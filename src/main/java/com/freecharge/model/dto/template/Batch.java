package com.freecharge.model.dto.template;

import javax.validation.constraints.NotNull;

import com.freecharge.model.dto.template.ColumnType.HEADER;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class Batch {
	private Header batchHeader = new Header(HEADER.BATCH);
	
	@NotNull
	private Body body;
	private Footer batchFooter;
	private String batchName;

}
