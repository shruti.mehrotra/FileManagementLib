package com.freecharge.model.dto.template;

import com.freecharge.model.dto.template.ColumnType.HEADER;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
@NoArgsConstructor
public class Header {
	private Column[] columns;
	private HEADER type;

	public Header(HEADER header) {
		this.type = header;
	}
}
