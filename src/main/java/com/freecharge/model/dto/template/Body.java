package com.freecharge.model.dto.template;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.freecharge.dao.dto.OnClause;
import com.freecharge.dao.dto.OrderByClause;
import com.freecharge.dao.dto.TableNameClause;
import com.freecharge.dao.dto.WhereClause;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class Body {

	@NotNull
	private TableNameClause tableNameClause;
	
	@NotEmpty
	private Column[] columns;
	
	@NotNull
	private WhereClause whereClauseWithStaticVal;

	private OnClause onClause;

	private OrderByClause orderByClause;
}
