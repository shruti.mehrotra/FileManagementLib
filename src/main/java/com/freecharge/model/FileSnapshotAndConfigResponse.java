package com.freecharge.model;

import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.FileSnapshotDto;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileSnapshotAndConfigResponse {
	private FileSnapshotDto fileSnapshotDto;
	private FileConfigSnapshotDto fileConfigSnapshotDto;
}
