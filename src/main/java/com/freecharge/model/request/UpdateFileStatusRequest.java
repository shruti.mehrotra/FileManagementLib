package com.freecharge.model.request;

import com.freecharge.model.type.FileStatus;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class UpdateFileStatusRequest implements ServiceRequest {
	private long fileId;
	private FileStatus fileStatus;
}
