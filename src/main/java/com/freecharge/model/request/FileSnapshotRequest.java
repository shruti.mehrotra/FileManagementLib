package com.freecharge.model.request;

import javax.validation.constraints.NotNull;

import com.freecharge.dao.dto.KeyValDtoList;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.model.type.FileConfigType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FileSnapshotRequest implements ServiceRequest {
	@NotNull
	private FileConfigType fileConfigType;
	
	@NotNull
	private KeyValWithDbOperatorList filterForSnapshotRecords;
	
	private KeyValDtoList keyValDtoList;
}
