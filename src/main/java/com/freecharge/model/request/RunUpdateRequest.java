package com.freecharge.model.request;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class RunUpdateRequest implements ServiceRequest {
	private long fileId;
}
