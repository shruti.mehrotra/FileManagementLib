package com.freecharge.model.request;

import javax.validation.constraints.NotNull;

import com.freecharge.model.dto.TimeRange;
import com.freecharge.model.type.FileConfigType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FileGenRequest implements ServiceRequest {
	@NotNull
	private FileConfigType fileConfigType;
	
	@NotNull
	private TimeRange updateTSRange;
}
