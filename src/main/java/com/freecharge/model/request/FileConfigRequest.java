package com.freecharge.model.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.freecharge.dao.entity.FileConfigEntity;
import com.freecharge.dao.entity.FileManagementEntity;
import com.freecharge.model.type.DestinationType;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileExtension;
import com.freecharge.model.type.FileFlowType;
import com.freecharge.model.type.FileStatus;
import com.freecharge.model.type.NotificationType;
import com.freecharge.util.GuidGenerator;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class FileConfigRequest implements ServiceRequest {
	@NotNull
	private FileConfigType fileConfigType;
	@NotBlank
	private String fileTemplate;
	@NotBlank
	private String filePathRegex;
	@NotBlank
	private String fileNameRegex;
	@NotNull
	private FileExtension fileExtension;
	@NotNull
	private FileFlowType fileFlowType;
	@NotNull
	private DestinationType destinationType;
	@NotNull
	private NotificationType notificationType;
	@NotEmpty
	private String notificationConfig;
	private long fileConfigId;
	@NotBlank
	private String destinationConfigFN;

	public FileManagementEntity toFileSnapshotEntity() {
		FileManagementEntity entity = new FileManagementEntity();
		entity.setFileId(GuidGenerator.getTimestampedGtid());
		entity.setFileConfigId(this.fileConfigId);
		entity.setFileStatus(FileStatus.INIT.name());
		entity.setFileConfigType(this.fileConfigType.name());
		return entity;
	}

	public FileConfigEntity toFileConfigSnapshotEntity() {
		FileConfigEntity entity = new FileConfigEntity();
		long fileConfigId = GuidGenerator.getTimestampedGtid();
		this.fileConfigId = fileConfigId;
		entity.setFileConfigId(this.fileConfigId);
		entity.setDestinationConfigFN(this.destinationConfigFN);
		entity.setDestinationType(destinationType.name());
		entity.setFileConfigType(fileConfigType.name());
		entity.setFileExtension(fileExtension.name());
		entity.setFileFlowType(fileFlowType.name());
		entity.setFileNameRegex(fileNameRegex);
		entity.setFilePathRegex(filePathRegex);
		entity.setFileTemplate(fileTemplate);
		entity.setNotificationType(notificationType.name());
		entity.setNotificationConfig(notificationConfig);
		
		return entity;
	}
}
