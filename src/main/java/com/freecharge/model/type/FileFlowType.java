package com.freecharge.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileFlowType {
	INBOUND, OUTBOUND;
}
