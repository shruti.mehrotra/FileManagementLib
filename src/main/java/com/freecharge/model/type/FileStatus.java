package com.freecharge.model.type;

import lombok.Getter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileStatus {
	INIT(0), 
	SNAPSHOT_START(1), 
	SNAPSHOT_FAILED(2), SNAPSHOT_NO_RECORD(3), 
	SNAPSHOT_COMPLETE(4),
	GEN_START(5), 
	GEN_FAILED(6), 
	GEN_COMPLETE(7), 
	UPLOAD_START(8), 
	UPLOAD_FAILED(9), 
	UPLOAD_COMPLETE(10),
	POST_UPLOAD_UPDATE_START(11),
	POST_UPLOAD_UPDATE_FAILED(12),
	POST_UPLOAD_UPDATE_COMPLETE(13),
	NOTIFICATION_START(14),
	NOTIFICATION_FAILED(15),
	NOTIFICATION_SENT(16);
	
	@Getter
	private Integer intValue;
	
	private FileStatus(Integer intValue) {
		this.intValue = intValue;
	}
}