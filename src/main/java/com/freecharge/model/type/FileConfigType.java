package com.freecharge.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileConfigType {
	REFUND_ICICI, REFUND_HDFC, REFUND_MIGS, REFUND_KOTAK, REFUND_FSS;
}
