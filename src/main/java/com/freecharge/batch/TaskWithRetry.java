package com.freecharge.batch;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
/**
 * Created by shruti.mehrotra on 19/5/16.
 */
public abstract class TaskWithRetry {

	@Value("${batchjob.retry.count}")
	private int maxRetryAttempts = 3;
	
	@Value("${task.backoff.interval}")
	private long backOffInterval = 1000;
	
	@Value("${task.backoff.multiplier}")
	private double backOffMultiplier = 2.0;

	protected abstract void task(Object... args) throws Exception;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void processWithRetry(final Object... args) throws Throwable {
		Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
		retryableExceptions.put(Exception.class, true);
		SimpleRetryPolicy policy = new SimpleRetryPolicy(maxRetryAttempts,
				retryableExceptions);
		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(policy);
		final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
		backOffPolicy.setInitialInterval(backOffInterval);
		backOffPolicy.setMultiplier(backOffMultiplier);
		retryTemplate.setBackOffPolicy(backOffPolicy);
		retryTemplate.execute(new RetryCallback() {

			@Override
			public Object doWithRetry(RetryContext context) throws Throwable {
				task(args);
				return null;
			}

		});

	}

}
