package com.freecharge.batch.handlers;

import java.util.Map;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.freecharge.batch.config.InMemoryBatchConfigurer;
import com.freecharge.batch.handlers.util.CleanUpJob;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by shruti.mehrotra on 19/5/16.
 */
@Slf4j
public abstract class MapBasedCleanableJobHandler {

	@Autowired
	protected InMemoryBatchConfigurer customBatchConfigurer;

	@Value("${batchjob.retry.count}")
	volatile int maxRetryAttempts;

	@Autowired
	private CleanUpJob cleanUpJob;

	public String JOB_FAIL_ATTEMPT_SEQ = "failAttemptSeq";
	
	public abstract Map<String, String> getJobData();

	protected void launchJob(Job job, JobParametersBuilder builder) {
		boolean failureFlag = false;
		int attemps = 0;
		while (attemps < maxRetryAttempts && (attemps == 0 || failureFlag)) {
			try {
				builder.addString(JOB_FAIL_ATTEMPT_SEQ,
						String.valueOf(attemps));
				attemps++;
				log.info("Running job: {} with attempt: {}", job.getName(), attemps);
				final JobExecution execution = customBatchConfigurer.getJobLauncher().run(job,
						builder.toJobParameters());
				if (ExitStatus.FAILED.equals(execution.getExitStatus()))
					throw new RuntimeException("Somthing went wrong in job");
			} catch (Throwable exception) {
				log.error(exception.getMessage());
				//cleanUpJob(builder.toJobParameters());
				failureFlag = true;
			}
		}
		log.info("Cleaning the job data for the job: " + job.getName());
		cleanUpJob.cleanUpJobData(job.getName());

	}

	/*private void cleanUpJob(JobParameters jobParameters) {
		String localFilePath = jobParameters
				.getString(BatchConstants.LOCAL_FILE_PATH);
		log.info("Cleaning following path:" + localFilePath);
		cleanUpJob.cleanUpPath(localFilePath);
	}*/

}
