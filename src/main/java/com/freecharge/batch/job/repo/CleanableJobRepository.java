package com.freecharge.batch.job.repo;
/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 20-Mar-2017
 */
public interface CleanableJobRepository {

	void clearJobData(String jobName);
}
