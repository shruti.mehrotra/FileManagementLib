package com.freecharge.batch.job.repo.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PostConstruct;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.repository.dao.ExecutionContextDao;
import org.springframework.batch.core.repository.dao.JobExecutionDao;
import org.springframework.batch.core.repository.dao.JobInstanceDao;
import org.springframework.batch.core.repository.dao.StepExecutionDao;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.batch.config.InMemoryBatchConfigurer;
import com.freecharge.batch.job.repo.CleanableJobRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) created on
 *         20-Mar-2017
 */
@Component("MapCleanableJobRepository")
@Slf4j
public class MapCleanableJobRepository implements CleanableJobRepository {

	private JobExecutionDao jobExecutionDao;
	private JobInstanceDao jobInstanceDao;
	private StepExecutionDao stepExecutionDao;
	private ExecutionContextDao executionContextDao;

	@Autowired
	InMemoryBatchConfigurer customBatchConfig;
	/*@Autowired*/
	MapJobRepositoryFactoryBean mapJobRepositoryFactoryBean;

	List<String> inProcessExitStatusList = new ArrayList<>(2);

	@PostConstruct
	public void initialize() {
		inProcessExitStatusList.add(ExitStatus.EXECUTING.getExitCode());
		inProcessExitStatusList.add(ExitStatus.UNKNOWN.getExitCode());
	}

	public void initBeans() {
		mapJobRepositoryFactoryBean = customBatchConfig.getMapJobRepository();
		jobExecutionDao = mapJobRepositoryFactoryBean.getJobExecutionDao();
		jobInstanceDao = mapJobRepositoryFactoryBean.getJobInstanceDao();
		stepExecutionDao = mapJobRepositoryFactoryBean.getStepExecutionDao();
		executionContextDao = mapJobRepositoryFactoryBean.getExecutionContextDao();

	}

	public void clearJobData(String jobName) {

		initBeans();
		try {
			cleanJobExecutionDao(jobExecutionDao, jobName);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			jobInstanceDao = mapJobRepositoryFactoryBean.getJobInstanceDao();
			log.error(
					"Could not clear the executionsByIdMap from memory.. This could lead to Out of memory issue!!!! Error: ",
					e);
		}
		try {
			cleanJobInstanceDao(jobInstanceDao, jobName);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.error(
					"Could not clear the jobInstancesMap from memory.. This could lead to Out of memory issue!!!! Error: ",
					e);
		}
		try {
			cleanStepExecutionDao(stepExecutionDao, jobName);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.error(
					"Could not clear the executionsByStepExecutionIdMap from memory.. This could lead to Out of memory issue!!!! Error: ",
					e);
		}
		try {
			cleanExecutionContextDao(executionContextDao, jobName);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.error(
					"Could not clear the contexts map from memory.. This could lead to Out of memory issue!!!! Error: ",
					e);
		}

	}

	@SuppressWarnings("unchecked")
	public void cleanJobExecutionDao(JobExecutionDao jobExecutionDao, String jobName)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field executionsByIdField = jobExecutionDao.getClass().getDeclaredField("executionsById");
		executionsByIdField.setAccessible(true);
		removeFinalModifier(executionsByIdField);
		ConcurrentHashMap<Long, JobExecution> executionsByIdMap = new ConcurrentHashMap<>();
		executionsByIdMap = (ConcurrentHashMap<Long, JobExecution>) executionsByIdField.get(jobExecutionDao);

		log.info("Before clearing executionsMap: " + executionsByIdMap);

		for (Map.Entry<Long, JobExecution> entry : executionsByIdMap.entrySet()) {
			JobExecution jobExecution = entry.getValue();
			if (!isJobExecuting(jobExecution.getExitStatus().getExitCode())) {
				executionsByIdMap.remove(entry.getKey());
			}
		}

		log.info("After clearing executionsMap: " + executionsByIdMap);
	}

	@SuppressWarnings("unchecked")
	public void cleanJobInstanceDao(JobInstanceDao jobInstanceDao, String jobName)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field jobInstancesField = jobInstanceDao.getClass().getDeclaredField("jobInstances");
		jobInstancesField.setAccessible(true);
		removeFinalModifier(jobInstancesField);
		ConcurrentHashMap<Long, JobInstance> jobInstancesMap = new ConcurrentHashMap<>();
		jobInstancesMap = (ConcurrentHashMap<Long, JobInstance>) jobInstancesField.get(jobInstanceDao);

		log.info("Before clearing jobInstancesMap: " + jobInstancesMap);

		for (Map.Entry<Long, JobInstance> entry : jobInstancesMap.entrySet()) {
			JobInstance jobInstance = entry.getValue();
			if (jobInstance.getJobName().equals(jobName)) {
				jobInstancesMap.remove(entry.getKey());
			}
		}

		log.info("After clearing jobInstancesMap: " + jobInstancesMap);
	}

	@SuppressWarnings("unchecked")
	public void cleanStepExecutionDao(StepExecutionDao stepExecutionDao, String jobName)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		try {
			Field executionsByJobExecutionIdField = stepExecutionDao.getClass()
					.getDeclaredField("executionsByJobExecutionId");
			executionsByJobExecutionIdField.setAccessible(true);
			removeFinalModifier(executionsByJobExecutionIdField);
			Map<Long, Map<Long, StepExecution>> executionsByJobExecutionIdMap = new ConcurrentHashMap<>();
			executionsByJobExecutionIdMap = (Map<Long, Map<Long, StepExecution>>) executionsByJobExecutionIdField
					.get(stepExecutionDao);

			log.info("Before clearing executionsByJobExecutionIdMap: " + executionsByJobExecutionIdMap);

			for (Entry<Long, Map<Long, StepExecution>> entry : executionsByJobExecutionIdMap.entrySet()) {
				Map<Long, StepExecution> stepExecutionMapInstance = entry.getValue();
				for (Map.Entry<Long, StepExecution> stepExeEntry : stepExecutionMapInstance.entrySet()) {
					if (!isJobExecuting(stepExeEntry.getValue().getJobExecution().getExitStatus().getExitCode())) {
						executionsByJobExecutionIdMap.remove(entry.getKey());
						break;
					}

				}
			}

			log.info("After clearing executionsByJobExecutionIdMap: " + executionsByJobExecutionIdMap);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.error(
					"Could not clear the executionsByJobExecutionIdMap from memory.. This could lead to Out of memory issue!!!! Error: ",
					e);
		}
		Field executionsByStepExecutionIdField = stepExecutionDao.getClass()
				.getDeclaredField("executionsByStepExecutionId");
		executionsByStepExecutionIdField.setAccessible(true);
		removeFinalModifier(executionsByStepExecutionIdField);
		Map<Long, StepExecution> executionsByStepExecutionIdMap = new ConcurrentHashMap<>();
		executionsByStepExecutionIdMap = (Map<Long, StepExecution>) executionsByStepExecutionIdField
				.get(stepExecutionDao);

		log.info("Before clearing executionsByStepExecutionIdMap: " + executionsByStepExecutionIdMap);

		for (Map.Entry<Long, StepExecution> entry : executionsByStepExecutionIdMap.entrySet()) {
			StepExecution stepExecution = entry.getValue();
			if (!isJobExecuting(stepExecution.getJobExecution().getExitStatus().getExitCode())) {
				executionsByStepExecutionIdMap.remove(entry.getKey());
			}
		}

		log.info("After clearing executionsByStepExecutionIdMap: " + executionsByStepExecutionIdMap);
	}

	@SuppressWarnings("unchecked")
	public void cleanExecutionContextDao(ExecutionContextDao executionContextDao, String jobName)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field contextsField = executionContextDao.getClass().getDeclaredField("contexts");
		contextsField.setAccessible(true);
		removeFinalModifier(contextsField);
		ConcurrentMap<Object, ExecutionContext> contextsMap = new ConcurrentHashMap<>();
		contextsMap = (ConcurrentMap<Object, ExecutionContext>) contextsField.get(executionContextDao);

		log.info("Before clearing contextsMap: " + contextsMap);

		contextsMap.clear();

		log.info("After clearing contextsMap: " + contextsMap);
	}

	public void removeFinalModifier(Field f)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		// Remove final modifier
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
	}

	public boolean isJobExecuting(String exitStatus) {
		return inProcessExitStatusList.contains(exitStatus);
	}
}
