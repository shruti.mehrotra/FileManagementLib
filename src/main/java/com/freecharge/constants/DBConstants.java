package com.freecharge.constants;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface DBConstants {
	public String FILE_ID = "FILE_ID";
	public String FILE_STATUS = "FILE_STATUS";
	public String FILE_TYPE = "FILE_TYPE";
	public String WHERE = " WHERE ";
	public String COMMA = ",";
	public String SET = " SET ";
	public String ON = " ON ";
	public String FROM = " FROM ";
	public String STAR = "*";
	public String SPACE = " ";
	public String SELECT = "SELECT ";
	public String ORDER_BY = " ORDER BY ";
	public String UPDATE = "UPDATE ";
	public String AND = " AND ";
	public String OR = " OR ";
	public String QUESTION_MARK = "?";
	public String REFUNDED_ON = "REFUNDED_ON";
	
	//File config table column name constants
	public String FILE_CONFIG_ID = "FILE_CONFIG_ID";
	public String FILE_CONFIG_TYPE = "FILE_CONFIG_TYPE";
	public String FILE_TEMPLATE = "FILE_TEMPLATE";
	public String FILE_PATH_REGEX = "FILE_PATH_REGEX";
	public String FILE_NAME_REGEX = "FILE_NAME_REGEX";
	public String FILE_EXTN = "FILE_EXTN";
	public String FILE_FLOW_TYPE = "FILE_FLOW_TYPE";
	public String DESTIN_TYPE = "DESTIN_TYPE";
	public String DESTIN_CONFIG_FN = "DESTIN_CONFIG_FN";
	public String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
	public String NOTIFICATION_CONFIG_FN = "NOTIFICATION_CONFIG_FN";
	public String CREATE_TS = "CREATE_TS";
	public String UPDATE_TS = "UPDATE_TS";
	
	//File Management table column name constants
	public String FILE_NAME = "FILE_NAME";
	public String REC_FILTER_HASH = "REC_FILTER_HASH";
	public String NUM_OF_RECORDS = "NUM_OF_RECORDS";
}
