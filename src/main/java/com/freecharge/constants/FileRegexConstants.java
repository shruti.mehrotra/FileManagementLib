package com.freecharge.constants;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface FileRegexConstants {
	public String FORWARD_SLASH = "/";
	public String BACKWARD_SLASH = "\\";
	public String DOT = ".";
	public String OPEN_TAG = "<";
	public String CLOSE_TAG = ">";
	public char SPACE_CHAR = '\u0000';
	public String COLON = ":";
	public String REQUEST = "request.";
	public String HYPHEN = "-";
}
