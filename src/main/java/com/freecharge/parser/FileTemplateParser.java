package com.freecharge.parser;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.exceptions.ExceptionErrorCode;
import com.freecharge.exceptions.InvalidFileConfigException;
import com.freecharge.exceptions.ValidationException;
import com.freecharge.file.IFileStatusManagementValidator;
import com.freecharge.model.dto.template.FileTemplate;
import com.freecharge.model.request.ServiceRequest;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component
public class FileTemplateParser {

	public FileTemplate getFileTemplateObjFromStr(String fileTemplateStr, long fileId, ServiceRequest request) {
		FileTemplate fileTemplate = null;
		if (!StringUtils.isEmpty(fileTemplateStr)) {
			// fileTemplate = new Gson().fromJson(fileTemplateStr,
			// FileTemplate.class);
			ObjectMapper mapper = new ObjectMapper();
			try {
				fileTemplate = mapper.readValue(fileTemplateStr, FileTemplate.class);
			} catch (IOException e) {
				throw new InvalidFileConfigException("Unparseable file template for file Id: " + fileId,
						ExceptionErrorCode.INVALID_FILE_CONFIG_TEMPLATE, e);
			}
		}
		// Check if template is valid..
		if (fileTemplate == null) {
			throw new InvalidFileConfigException("Unparseable file template for file Id: " + fileId,
					ExceptionErrorCode.INVALID_FILE_CONFIG_TEMPLATE);
		}

		IFileStatusManagementValidator validatorClass = fileTemplate.getValidatorClass();
		if (validatorClass != null) {
			try {
				validatorClass.validateRequest(request);
			} catch (Exception e) {
				throw new ValidationException("Validation failed for file Id: " + fileId,
						ExceptionErrorCode.BUSINESS_VALIDATION_FAILED, e);
			}
		}
		return fileTemplate;
	}
	
	public static void main(String[] args) {
		FileTemplateParser parser = new FileTemplateParser();
		String fileTemplateStr = "{\"batches\": [ { \"batchName\": \"GT200\", \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" } ], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\", \"pairSeparator\": null } ] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_amount\", \"value\": \"200\", \"operator\": \"GREATER_THAN\", \"pairSeparator\": \"AND\" } ] } }, \"columns\": [ { \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" } ], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_amount\", \"value\": \"DESC\", \"operator\": \"SPACE\", \"pairSeparator\": null } ] } } } }, { \"batchName\": \"LT200\", \"batchHeader\": { \"columns\": [ { \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" } ], \"type\": \"BATCH\" }, \"body\": { \"tableNameClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_transaction\", \"value\": \"refund_txn\", \"operator\": \"AS\", \"pairSeparator\": null } ] } }, \"whereClauseWithStaticVal\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_amount\", \"value\": \"200\", \"operator\": \"LESS_THAN\", \"pairSeparator\": \"AND\" } ] } }, \"columns\": [ { \"id\": \"col1\", \"name\": \"transaction_id\" }, { \"id\": \"col2\", \"name\": \"reference_id\" }, { \"id\": \"col3\", \"name\": \"idempotency_id\" } ], \"orderByClause\": { \"keyValPairs\": { \"keyValPairWithDbOperatorList\": [ { \"key\": \"refund_amount\", \"value\": \"DESC\", \"operator\": \"SPACE\", \"pairSeparator\": null } ] } } } } ] }";
		long fileId = 12345667L;
		ServiceRequest request = null;
		System.out.println(parser.getFileTemplateObjFromStr(fileTemplateStr, fileId, request).toString());
	}
}
