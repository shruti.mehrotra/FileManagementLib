package com.freecharge.trigger.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.model.request.FileConfigRequest;
import com.freecharge.model.request.FileGenRequest;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.request.RunUpdateRequest;
import com.freecharge.model.request.UpdateFileStatusRequest;
import com.freecharge.model.response.FileGenListResponse;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.trigger.service.TriggerService;
import com.freecharge.util.ResponseGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
//@RestController
@Slf4j
public class TriggerController {

	@Autowired
	private TriggerService triggerService;

	/*@RequestMapping(value = "/triggerFileStatusManagementJob", method = RequestMethod.GET)
	public void triggerFileStatusManagementJob() {
		try {
			triggerService.triggerJob();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	@RequestMapping(value = "/triggerFileGenerationBySnapshotRequest", method = RequestMethod.POST)
	public @ResponseBody FileGenResponse triggerFileGenerationBySnapshotRequest(@Valid @RequestBody FileSnapshotRequest request) {
		return triggerService.triggerFileGenerationBySnapshotRequest(request);
	}
	
	@RequestMapping(value = "/triggerFileGenerationByTimeRange", method = RequestMethod.POST)
	public @ResponseBody FileGenListResponse triggerFileGenerationByTimeRange(@Valid @RequestBody FileGenRequest request) {
		return triggerService.triggerFileGenerationByTimeRange(request);
	}
	
	@RequestMapping(value = "/snapshotFile", method = RequestMethod.POST)
	public ServiceResponse snapshotFile(@Valid @RequestBody FileSnapshotRequest request) {
		return triggerService.snapshotFile(request);
	}
	
	@RequestMapping(value = "/configureFileSnapshot", method = RequestMethod.POST)
	public FileGenResponse configureFileSnapshot(@Valid @RequestBody FileConfigRequest request) {
		return triggerService.configureFileSnapshot(request);
	}
	
	@RequestMapping(value = "/postUploadUpdate", method = RequestMethod.POST)
	public ServiceResponse postUploadUpdate(@Valid @RequestBody RunUpdateRequest request) {
		return triggerService.postUploadUpdate(request);
	}
	
	@RequestMapping(value = "/updateFileStatus", method = RequestMethod.POST)
	public ServiceResponse updateFileStatus(@Valid @RequestBody UpdateFileStatusRequest request) {
		boolean status = false;
		ServiceResponse response = new ServiceResponse();
		try {
			status = triggerService.updateFileStatus(request.getFileStatus(), request.getFileId());
			if (status) {
				response = ResponseGenUtil.getSuccessServiceResponse("Status updated successfully!");
			} else {
				response = ResponseGenUtil.getFailureServiceResponse(new FileManagementLibException());
			}
		} catch (FileManagementLibException e) {
			log.error("Error occured in updating file status for the request {}. Error: {}", request.toString(), e);
			response = ResponseGenUtil.getFailureServiceResponse(e);
		}
		return response;
	}
}
