package com.freecharge.trigger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.file.generator.IFileGeneratorService;
import com.freecharge.file.snapshot.IFileSnapshotter;
import com.freecharge.file.snapshot.impl.FileSnapshotUpdateServiceImpl;
import com.freecharge.file.updator.IPostUploadUpdateService;
import com.freecharge.model.request.FileConfigRequest;
import com.freecharge.model.request.FileGenRequest;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.request.RunUpdateRequest;
import com.freecharge.model.response.FileGenListResponse;
import com.freecharge.model.response.FileGenResponse;
import com.freecharge.model.response.ServiceResponse;
import com.freecharge.model.type.FileStatus;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
@Service("TriggerService")
public class TriggerService {

	@Autowired
	private FileSnapshotUpdateServiceImpl snapshotUpdateService;

	@Autowired
	@Qualifier("FileSnapshotterImpl")
	private IFileSnapshotter fileSnapshotter;
	
	@Autowired
	@Qualifier("FileGeneratorServiceImpl")
	private IFileGeneratorService fileGeneratorService;
	
	@Autowired
	@Qualifier("PostUploadUpdateServiceImpl")
	private IPostUploadUpdateService uploadUpdateService;

	public FileGenResponse triggerFileGenerationBySnapshotRequest(FileSnapshotRequest request) {
		return fileGeneratorService.generateFileBySnapshotRequest(request);
	}
	
	public FileGenListResponse triggerFileGenerationByTimeRange(FileGenRequest request) {
		return fileGeneratorService.generateFileByTimeRange(request);
	}

	public FileGenResponse configureFileSnapshot(FileConfigRequest request) {
		return snapshotUpdateService.insert(request);
	}

	public boolean updateFileStatus(FileStatus fileStatus, long fileId) {
		return snapshotUpdateService.updateFileStatus(fileStatus, fileId);
	}

	public ServiceResponse snapshotFile(FileSnapshotRequest request) {
		return fileSnapshotter.doSnapshot(request);
	}
	
	public ServiceResponse postUploadUpdate(RunUpdateRequest request) {
		return uploadUpdateService.runUpdate(request);
	}
}
