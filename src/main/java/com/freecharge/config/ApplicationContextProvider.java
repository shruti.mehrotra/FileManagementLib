package com.freecharge.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class ApplicationContextProvider {
	public static ApplicationContext getApplicationContext() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:dsImport.xml");
		return ctx;
	}
}
