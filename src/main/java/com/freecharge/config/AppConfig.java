package com.freecharge.config;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.freecharge.dao.config.DatabaseConfiguration;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Configuration
@ImportResource("classpath:spring/fileapplication-context.xml")
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, BatchAutoConfiguration.class})
@EnableBatchProcessing
public class AppConfig extends DatabaseConfiguration {
	
}
