package com.freecharge.dao.entity;

import org.springframework.context.ApplicationContext;

import com.freecharge.config.ApplicationContextProvider;
import com.freecharge.model.dto.FileConfigSnapshotDto;
import com.freecharge.model.dto.S3Config;
import com.freecharge.model.type.DestinationType;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileExtension;
import com.freecharge.model.type.FileFlowType;
import com.freecharge.model.type.NotificationType;

import lombok.Data;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) created on
 *         17-Mar-2017
 */

@Data
public class FileConfigEntity {
	private long fileConfigId;
	private String fileConfigType;
	private String fileTemplate;
	private String filePathRegex;
	private String fileNameRegex;
	private String fileExtension;
	private String fileFlowType;
	private String destinationType;
	private String destinationConfigFN;
	private String notificationType;
	private String notificationConfig;
	private String creationTS;
	private String updateTS;

	public FileConfigSnapshotDto toDto() {
		FileConfigSnapshotDto dto = new FileConfigSnapshotDto();
		dto.setFileConfigId(this.fileConfigId);
		dto.setFileConfigType(FileConfigType.valueOf(this.fileConfigType));
		dto.setFileTemplate(this.fileTemplate);
		dto.setFilePathRegex(this.filePathRegex);
		dto.setFileNameRegex(this.fileNameRegex);
		dto.setFileExtension(FileExtension.valueOf(this.fileExtension));
		dto.setFileFlowType(FileFlowType.valueOf(this.fileFlowType));
		dto.setDestinationType(DestinationType.valueOf(this.destinationType));
		dto.setS3Config(getS3ConfigByName(this.destinationConfigFN));
		dto.setDestinationConfigFN(this.destinationConfigFN);
		dto.setNotificationType(NotificationType.valueOf(this.notificationType));
		dto.setNotificationConfig(this.notificationConfig);
		dto.setCreateTS(this.creationTS);
		dto.setUpdateTS(this.updateTS);
		return dto;
	}

	private S3Config getS3ConfigByName(String name) {
		S3Config config = new S3Config();

		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		switch (destinationType) {
		case "S3":
			config = (S3Config) ctx.getBean(name);
			break;
		default:
			break;
		}
		return config;
	}
}
