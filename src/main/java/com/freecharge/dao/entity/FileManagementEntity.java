package com.freecharge.dao.entity;

import com.freecharge.model.dto.FileSnapshotDto;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.model.type.FileStatus;
import com.freecharge.util.GuidGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */

@Data
@NoArgsConstructor
public class FileManagementEntity {
	
	private long fileId;
	private String fileName;
	private String fileConfigType;
	private String fileStatus;
	private String creationTS;
	private String updateTS;
	private long fileConfigId;
	private String recFilterHash;
	private long numOfRecords;
	
	public FileManagementEntity(FileConfigType fileConfigType, long fileConfigId, FileStatus fileStatus, String hash, String fileName) {
		this.fileConfigType = fileConfigType.name();
		this.fileConfigId = fileConfigId;
		this.fileStatus = fileStatus.name();
		this.recFilterHash = hash;
		this.fileId = GuidGenerator.getTimestampedGtid();
		this.fileName = fileName;
	}
	
	public FileSnapshotDto toDto() {
		FileSnapshotDto fileState = new FileSnapshotDto();
		fileState.setFileId(this.fileId);
		fileState.setFileName(this.fileName);
		fileState.setFileStatus(FileStatus.valueOf(this.fileStatus));
		fileState.setCreationTS(this.creationTS);
		fileState.setUpdateTS(this.updateTS);
		fileState.setFileConfigId(this.fileConfigId);
		fileState.setRecFilterHash(this.recFilterHash);
		fileState.setNumOfRecords(this.numOfRecords);
		return fileState;
	}

}
