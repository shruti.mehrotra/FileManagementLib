package com.freecharge.dao.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * Created by shruti.mehrotra on 19/5/16.
 */
public class DatabaseConfiguration {

	@Value("${database.url}")
	private String url;

	@Value("${database.userName}")
	private String userName;
	
	@Value("${database.password}")
	private String password;
	
	@Value("${database.name}")
	private String dbName;

	public String getUrl() {
		return url;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getDbName() {
		return dbName;
	}

	/*public String getDBUrl() {
		return DBUtil.getDBUrl(host, port, dbName);
	}*/
}
