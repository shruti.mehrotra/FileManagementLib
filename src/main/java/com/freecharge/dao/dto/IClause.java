package com.freecharge.dao.dto;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IClause {
	public String toString();
}
