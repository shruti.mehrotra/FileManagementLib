package com.freecharge.dao.dto.operators;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum PairSeparator {
	
	NATURAL_JOIN(" JOIN "), LEFT_OUTER_JOIN(" LEFT OUTER JOIN "), RIGHT_OUTER_JOIN(" RIGHT OUTER JOIN "),
	FULL_JOIN(" FULL JOIN "), AND(" AND "), OR(" OR "), COMMA(",");
	
	@Setter
	@Getter
	protected String separatorSymbol;
	
	PairSeparator(String separatorSymbol) {
		this.separatorSymbol = separatorSymbol;
	}
}
