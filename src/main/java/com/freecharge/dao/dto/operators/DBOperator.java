package com.freecharge.dao.dto.operators;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum DBOperator {
	EQUALS(" = ", ".equals(?VAL)"),
	LIKE(" LIKE ",".contains(?VAL)"),
	NOT_EQUALS(" <> ", "?KEY != ?VAL"),
	GREATER_THAN(" > ", "?KEY > ?VAL "),
	GREATER_THAN_EQUAL_TO(" >= ", "?KEY >= ?VAL"),
	LESS_THAN(" < ", "?KEY < ?VAL"),
	LESS_THAN_EQUAL_TO(" <= ","?KEY <= ?VAL"),
	IN(" IN ", ".contains(?VAL)"),
	AS(" AS ", " "),
	DOT(".", " "),
	SPACE(" ", " "),
	IS_NULL(" IS NULL ", "?KEY == ?VAL");
	
	@Getter
	private String op;
	@Getter
	private String javaOp;
	DBOperator(String op, String javaOp) {
		this.op = op;
		this.javaOp = javaOp;
	}
	
	private static final Map<String,DBOperator> map;
    static {
        map = new HashMap<String,DBOperator>();
        for (DBOperator v : DBOperator.values()) {
            map.put(v.op, v);
        }
    }
	
	public static String getOpByValue(String value) {
		String name = null;
		DBOperator dbOp = map.get(value);
		if (dbOp != null) {
			name = dbOp.getOp();
		}
		/*DBOperator[] dbOpArr = DBOperator.values();
		
		for (DBOperator dbOp: dbOpArr) {
			if (dbOp.getOp() == value) {
				name = dbOp.name();
				break;
			}
		}*/
		return name;
	}
}
