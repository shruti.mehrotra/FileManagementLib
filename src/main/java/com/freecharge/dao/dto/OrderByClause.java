package com.freecharge.dao.dto;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.dto.operators.PairSeparator;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class OrderByClause implements IClause {
	KeyValWithDbOperatorList keyValPairs;
	
	@Override
	public String toString() {
		StringBuilder onClause = new StringBuilder();
		onClause.append(DBConstants.ORDER_BY)
		.append(keyValPairs.toString(PairSeparator.COMMA));
		return onClause.toString();
	}
}
