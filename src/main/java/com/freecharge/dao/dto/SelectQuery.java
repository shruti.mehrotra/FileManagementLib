package com.freecharge.dao.dto;

import org.apache.commons.lang3.StringUtils;

import com.freecharge.constants.DBConstants;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@NoArgsConstructor
public class SelectQuery {

	private String[] columnsToSelect;
	private TableNameClause tableNameClause;
	private OnClause onClause;
	private WhereClause whereClause;
	private OrderByClause orderByClause;

	public SelectQuery(String[] columns, TableNameClause tableNameClause, WhereClause whereClause) {
		this.columnsToSelect = columns;
		this.tableNameClause = tableNameClause;
		this.whereClause = whereClause;
	}

	public SelectQuery(String[] columns, TableNameClause tableNameClause, WhereClause whereClause, OnClause onClause) {
		this(columns, tableNameClause, whereClause);
		this.onClause = onClause;
	}

	public SelectQuery(String[] columns, TableNameClause tableNameClause, WhereClause whereClause, OnClause onClause,
			OrderByClause orderByClause) {
		this(columns, tableNameClause, whereClause, onClause);
		this.orderByClause = orderByClause;
	}

	public SelectQuery(String[] columns, TableNameClause tableNameClause, WhereClause whereClause,
			OrderByClause orderByClause) {
		this(columns, tableNameClause, whereClause);
		this.orderByClause = orderByClause;
	}

	public String toString() {
		StringBuilder selectQuery = new StringBuilder();
		selectQuery.append(DBConstants.SELECT);

		if (columnsToSelect != null && columnsToSelect.length > 0) {
			int count = 1;
			for (String column : columnsToSelect) {
				if (!StringUtils.isEmpty(column)) {
					selectQuery.append(column);
					if (count != columnsToSelect.length) {
						selectQuery.append(",");
					}
				}
				count++;
			}
		} else {
			selectQuery.append(DBConstants.STAR);
		}
		selectQuery.append(DBConstants.SPACE).append(DBConstants.FROM);
		if (tableNameClause != null) {
			selectQuery.append(tableNameClause.toString());
		}
		if (onClause != null) {
			selectQuery.append(onClause.toString());
		}
		if (whereClause != null) {
			selectQuery.append(whereClause.toString());
		}
		if (orderByClause != null) {
			selectQuery.append(orderByClause.toString());
		}
		return selectQuery.toString();
	}
}
