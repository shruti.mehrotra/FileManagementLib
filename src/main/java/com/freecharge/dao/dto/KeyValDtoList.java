package com.freecharge.dao.dto;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import java.util.List;

import lombok.Data;

@Data
public class KeyValDtoList {
	private List<KeyValDto> keyValDtoList;
}
