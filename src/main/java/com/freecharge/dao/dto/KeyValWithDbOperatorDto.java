package com.freecharge.dao.dto;

import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.dao.dto.operators.PairSeparator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValWithDbOperatorDto {
	private String key;
	private String value;
	private DBOperator operator;
	private PairSeparator pairSeparator;
}
