package com.freecharge.dao.dto;

import com.freecharge.constants.DBConstants;

import lombok.Data;

/**
 * 
 * @author standby
 *
 */
@Data
public class FromClause implements IClause {
	private TableNameClause tableNameClause;
	
	@Override
	public String toString() {
		StringBuilder fromClause = new StringBuilder();
		fromClause.append(DBConstants.FROM)
					.append(tableNameClause.toString());
		return fromClause.toString();
	}
}
