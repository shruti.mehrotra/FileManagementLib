package com.freecharge.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValDto {
	private String key;
	private String value;
}
