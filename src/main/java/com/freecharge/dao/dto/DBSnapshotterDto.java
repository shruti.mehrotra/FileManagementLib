package com.freecharge.dao.dto;

import java.util.List;

import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@ToString
public class DBSnapshotterDto extends DBQueryDto {

	private List<KeyValWithDbOperatorDto> columnToUpdate;

	public List<KeyValWithDbOperatorDto> getColumnToUpdate() {
		return columnToUpdate;
	}

	public void setColumnToUpdate(List<KeyValWithDbOperatorDto> columnToUpdate) {
		this.columnToUpdate = columnToUpdate;
	}
	
	public DBSnapshotterDto(DBQueryDto dto) {
		super.setBatchFooter(dto.getBatchFooter());
		super.setBatchHeader(dto.getBatchHeader());
		super.setDbName(dto.getDbName());
		super.setDbUrl(dto.getDbUrl());
		super.setPassword(dto.getPassword());
		super.setUserName(dto.getUserName());
		super.setTableNameClause(dto.getTableNameClause());
		super.setWhereClause(dto.getWhereClause());
		super.setOnClause(dto.getOnClause());
	}

}
