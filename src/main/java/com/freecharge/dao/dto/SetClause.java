package com.freecharge.dao.dto;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.dto.operators.PairSeparator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetClause implements IClause {
	KeyValWithDbOperatorList keyValWithDbOperatorList;

	@Override
	public String toString() {
		StringBuilder setClause = new StringBuilder();
		setClause.append(DBConstants.SET);
		setClause.append(keyValWithDbOperatorList.toString(PairSeparator.COMMA));
		return setClause.toString();
	}
}
