package com.freecharge.dao.dto;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.dao.dto.operators.PairSeparator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WhereClause implements IClause {
	KeyValWithDbOperatorList keyValPairs;

	@Override
	public String toString() {
		StringBuilder whereClause = new StringBuilder();
		whereClause.append(DBConstants.WHERE);
		whereClause.append(keyValPairs.toString(PairSeparator.AND));
		return whereClause.toString();
	}

	public boolean applyAsJava() {
		boolean isApplicable = false;
		if (this.keyValPairs != null) {
			for (KeyValWithDbOperatorDto dto : this.keyValPairs.getKeyValPairWithDbOperatorList()) {
				isApplicable = applyDBOperatorToKeyAnVal(dto);
				if (isApplicable) {
					continue;
				} else {
					break;
				}
			}
		}
		return isApplicable;
	}

	private boolean applyDBOperatorToKeyAnVal(KeyValWithDbOperatorDto dto) {
		String key = dto.getKey();
		String value = dto.getValue();
		DBOperator dbOperator = dto.getOperator();

		String expression = dbOperator.getJavaOp().replace("?KEY", key).replace("?VAL", value);
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    try {
	    	boolean result = (boolean) engine.eval(expression);
			System.out.println(result);
			return result;
		} catch (ScriptException e) {
			return false;
		}
	}
}
