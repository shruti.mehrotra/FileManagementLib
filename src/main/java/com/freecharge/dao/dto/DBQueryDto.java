package com.freecharge.dao.dto;

import com.freecharge.model.dto.template.Column;
import com.freecharge.model.dto.template.Footer;
import com.freecharge.model.dto.template.Header;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DBQueryDto {
	private String dbUrl;
	private String userName;
	private String password;
	private String dbName;
	private TableNameClause tableNameClause;
	private WhereClause whereClause;
	private OnClause onClause;
	private Header batchHeader;
	private Footer batchFooter;
	private String batchSheetName;
	private String[] columnsToSelect;
	private Column[] allColumns;
}
