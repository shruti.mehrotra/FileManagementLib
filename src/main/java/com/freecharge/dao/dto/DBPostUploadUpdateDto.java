package com.freecharge.dao.dto;

import com.freecharge.model.dto.template.DBConfig;
import com.freecharge.model.dto.template.PostUploadDBUpdate;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class DBPostUploadUpdateDto {

	PostUploadDBUpdate postUploadDBUpdateDto;
	DBConfig dbConfig;

}
