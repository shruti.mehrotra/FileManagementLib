package com.freecharge.dao.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.freecharge.dao.dto.operators.PairSeparator;
import com.freecharge.exceptions.ExceptionErrorCode;
import com.freecharge.exceptions.InvalidFileConfigException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValWithDbOperatorList {
	@NotNull
	@NotEmpty
	List<KeyValWithDbOperatorDto> keyValPairWithDbOperatorList;
	
	public String toString() {
		return toString(PairSeparator.COMMA);
	}
	
	public String toString(PairSeparator defaultPairSeparator) {
		StringBuilder pairClause = new StringBuilder();

		int count = keyValPairWithDbOperatorList.size();
		for (KeyValWithDbOperatorDto keyValPair : keyValPairWithDbOperatorList) {
			String dbOp = keyValPair.getOperator().getOp();
			pairClause.append(keyValPair.getKey()).append(dbOp).append(keyValPair.getValue());
			if (count > 1) {
				if (keyValPair.getPairSeparator() != null) {
					pairClause.append(keyValPair.getPairSeparator().getSeparatorSymbol());
				} else {
					if (defaultPairSeparator != null) {
						pairClause.append(defaultPairSeparator.getSeparatorSymbol());
					} else {
						throw new InvalidFileConfigException(
								"Pair Separator not provided either with template or default!",
								ExceptionErrorCode.INVALID_FILE_CONFIG_TEMPLATE);
					}
				}
			}
			count--;
		}
		return pairClause.toString();
	}
}
