package com.freecharge.dao.dto;

import com.freecharge.constants.DBConstants;
import com.freecharge.exceptions.QueryExecutionException;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@AllArgsConstructor
public class UpdateQuery {

	TableNameClause tableNameClause;
	SetClause setClause;
	WhereClause whereClause;
	OnClause onClause;

	public String toString() {
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append(DBConstants.UPDATE);
		if (tableNameClause != null && tableNameClause.getKeyValPairs() != null
				&& !tableNameClause.getKeyValPairs().getKeyValPairWithDbOperatorList().isEmpty()) {
			updateQuery.append(tableNameClause.toString());
		} else {
			throw new QueryExecutionException("TableName clause is mandatory for update query!");
		}
		if (onClause != null && onClause.getKeyValPairs() != null
				&& !onClause.getKeyValPairs().getKeyValPairWithDbOperatorList().isEmpty()) {
			updateQuery.append(onClause.toString());
		}
		if (setClause != null && setClause.getKeyValWithDbOperatorList() != null
				&& !setClause.getKeyValWithDbOperatorList().getKeyValPairWithDbOperatorList().isEmpty()) {
			updateQuery.append(setClause.toString());
		} else {
			throw new QueryExecutionException("Set clause is mandatory for update query!");
		}
		if (whereClause != null && whereClause.getKeyValPairs() != null
				&& !whereClause.getKeyValPairs().getKeyValPairWithDbOperatorList().isEmpty()) {
			updateQuery.append(whereClause.toString());
		} else {
			throw new QueryExecutionException("Where clause is mandatory for update query!");
		}
		
		return updateQuery.toString();
	}
}
