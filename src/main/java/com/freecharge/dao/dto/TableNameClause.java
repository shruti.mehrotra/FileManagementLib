package com.freecharge.dao.dto;

import com.freecharge.dao.dto.operators.PairSeparator;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class TableNameClause implements IClause {
	KeyValWithDbOperatorList keyValPairs;
	
	@Override
	public String toString() {
		StringBuilder tableNameClause = new StringBuilder();
		tableNameClause.append(keyValPairs.toString(PairSeparator.NATURAL_JOIN));
		return tableNameClause.toString();
	}
}
