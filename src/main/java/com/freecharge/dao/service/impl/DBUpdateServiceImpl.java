package com.freecharge.dao.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.DBUtil;
import com.freecharge.dao.dto.DBPostUploadUpdateDto;
import com.freecharge.dao.dto.DBSnapshotterDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.dao.dto.SetClause;
import com.freecharge.dao.dto.TableNameClause;
import com.freecharge.dao.dto.UpdateQuery;
import com.freecharge.dao.dto.WhereClause;
import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.dao.dto.operators.PairSeparator;
import com.freecharge.dao.service.IDBUpdateService;
import com.freecharge.exceptions.QueryExecutionException;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.model.dto.template.PostUploadDBUpdate;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("DBUpdateServiceImpl")
public class DBUpdateServiceImpl extends DBConnectionService implements IDBUpdateService {

	private Connection con;
	private PreparedStatement stmt;

	@Override
	public long postDBUpdate(DBPostUploadUpdateDto dto, FileSnapshotAndConfigResponse response) throws SQLException {
		long numOfRecordsUpdated = 0;
		con = getConnection(dto.getDbConfig());
		
		PostUploadDBUpdate updateDto = dto.getPostUploadDBUpdateDto();
		TableNameClause tableNameClause = updateDto.getTableNameClause();
		WhereClause whereClause = updateDto.getWhereClause();
		SetClause setClause = updateDto.getSetClause();
		
		UpdateQuery updateQuery = new UpdateQuery(tableNameClause, setClause, whereClause, updateDto.getOnClause());
		String sql = updateQuery.toString();
		executeUpdate(response, numOfRecordsUpdated, sql);
		return numOfRecordsUpdated;
	}
	
	@Override
	public long snapshotUpdate(DBSnapshotterDto dto, FileSnapshotAndConfigResponse response,
			KeyValWithDbOperatorList filterForSnapshotRecords) {
		long numOfRecordsUpdated = 0;
		con = getConnection(dto);
		TableNameClause tableNameClause = dto.getTableNameClause();
		SetClause setClause = new SetClause(new KeyValWithDbOperatorList(dto.getColumnToUpdate()));
		WhereClause whereClause = dto.getWhereClause();
		updateWhereClause(filterForSnapshotRecords, whereClause);
		
		// executing UPDATE query
		UpdateQuery updateQuery = new UpdateQuery(tableNameClause, setClause, whereClause, dto.getOnClause());
		String sql = updateQuery.toString();

		numOfRecordsUpdated = executeUpdate(response, numOfRecordsUpdated, sql);
		return numOfRecordsUpdated;
	}
	
	private long executeUpdate(FileSnapshotAndConfigResponse response, long numOfRecordsUpdated, String sql) {
		try {
			stmt = DBUtil.getPreparedStatement(sql, response, con);
			System.out.println(stmt);
			numOfRecordsUpdated = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new QueryExecutionException("Unable to execute the following query: " + sql, e);
		} finally {
			// close connection, stmt
			try {
				con.close();
			} catch (SQLException se) {
				/* can't do anything */
			}
			try {
				stmt.close();
			} catch (SQLException se) {
				/* can't do anything */
			}
		}
		return numOfRecordsUpdated;
	}

	private void updateWhereClause(KeyValWithDbOperatorList filterForSnapshotRecords, WhereClause whereClause) {
		if (filterForSnapshotRecords != null && !filterForSnapshotRecords.getKeyValPairWithDbOperatorList().isEmpty()) {
			whereClause.getKeyValPairs().getKeyValPairWithDbOperatorList()
			.addAll(filterForSnapshotRecords.getKeyValPairWithDbOperatorList());
		}
		whereClause.getKeyValPairs().getKeyValPairWithDbOperatorList().addAll(getDefaultListForSnapshotUpdate());

	}

	private List<KeyValWithDbOperatorDto> getDefaultListForSnapshotUpdate() {
		List<KeyValWithDbOperatorDto> defaultList = new ArrayList<>();
		defaultList.add(new KeyValWithDbOperatorDto(DBConstants.FILE_ID, "", DBOperator.IS_NULL, PairSeparator.OR));
		defaultList.add(new KeyValWithDbOperatorDto(DBConstants.FILE_ID, "''", DBOperator.EQUALS, null));
		return defaultList;
	}


}
