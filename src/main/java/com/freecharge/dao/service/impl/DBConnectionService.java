package com.freecharge.dao.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.dao.config.DatabaseConfiguration;
import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.exceptions.DatabaseConnectionException;
import com.freecharge.model.dto.template.DBConfig;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component
public class DBConnectionService {
	
	@Autowired
	DatabaseConfiguration dbConfig;
	
	public Connection getConnection(DBQueryDto dto) {
		Connection con;
		String url = dto.getDbUrl();// DBUtil.getDBUrl(dto.getHost(),
									// dto.getDbName());
		String password = dto.getPassword();
		String userName = dto.getUserName();
		try {
			// opening database connection to MySQL server
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con = DriverManager.getConnection(url, userName, password);
			con.setAutoCommit(true);
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException(
					"Cannot connect to the database with the following config: " + dto.toString(), sqlEx);
		}
		return con;
	}

	public Connection getConnection(DBConfig dto) {
		Connection con;
		String url = dto.getDbUrl();// DBUtil.getDBUrl(dto.getHost(),
									// dto.getDbName());
		String password = dto.getPassword();
		String userName = dto.getUserName();
		try {
			// opening database connection to MySQL server
			con = DriverManager.getConnection(url, userName, password);
			con.setAutoCommit(true);
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException(
					"Cannot connect to the database with the following config: " + dto.toString(), sqlEx);
		}
		return con;
	}
	
	public Connection getFMLConnection() {
		Connection con;
		String url = dbConfig.getUrl();
		String password = dbConfig.getPassword();
		String userName = dbConfig.getUserName();
		try {
			// opening database connection to MySQL server
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con = DriverManager.getConnection(url, userName, password);
			con.setAutoCommit(true);
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException(
					"Cannot connect to the FML database: " , sqlEx);
		}
		return con;
	}
	
	public Statement getStatement(Connection con) {
		Statement stmt = null;
		try {
			// getting Statement object to execute query
			stmt = con.createStatement();
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException("Cannot connect to the database: ", sqlEx);
		}

		return stmt;
	}
}
