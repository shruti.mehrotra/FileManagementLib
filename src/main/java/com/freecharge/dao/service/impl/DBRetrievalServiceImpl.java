package com.freecharge.dao.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.freecharge.dao.DBUtil;
import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.DBQueryResponse;
import com.freecharge.dao.dto.SelectQuery;
import com.freecharge.dao.service.IDBRetrievalService;
import com.freecharge.exceptions.QueryExecutionException;
import com.freecharge.model.FileSnapshotAndConfigResponse;


/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("DBRetrievalServiceImpl")
public class DBRetrievalServiceImpl extends DBConnectionService implements IDBRetrievalService {

	private Connection con;
	private ResultSet rs;
	private PreparedStatement stmt;
	
	@Override
	public DBQueryResponse retrieveData(DBQueryDto dto, FileSnapshotAndConfigResponse snapshotResponse) {
		DBQueryResponse response = new DBQueryResponse();
		con = getConnection(dto);
		String sql = null;
		// executing SELECT query
		try {
			SelectQuery selectQuery = new SelectQuery(dto.getColumnsToSelect(), dto.getTableNameClause(), dto.getWhereClause(), dto.getOnClause());
			
			sql = selectQuery.toString();
			stmt = DBUtil.getPreparedStatement(sql, snapshotResponse, con);
			rs = stmt.executeQuery();
			/*while (rs.next()) {
				for (int i = 0; i < dto.getColumns().length; i++) {
					System.out.println("Record : " + rs.getString(i));
				}
			}*/
		} catch (SQLException e) {
			throw new QueryExecutionException("Unable to execute the following query: " + sql, e);
		} 
		response.setCon(con);
		response.setStmt(stmt);
		response.setRs(rs);
		return response;

	}

	public DBQueryResponse retrieveData(String query) {
		DBQueryResponse response = new DBQueryResponse();
		con = getFMLConnection();
		// executing SELECT query
		try {
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			throw new QueryExecutionException("Unable to execute the following query: " + query, e);
		} 
		response.setCon(con);
		response.setStmt(stmt);
		response.setRs(rs);
		return response;

	}
	
}
