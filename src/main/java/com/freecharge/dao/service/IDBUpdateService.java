package com.freecharge.dao.service;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import java.sql.SQLException;

import com.freecharge.dao.dto.DBPostUploadUpdateDto;
import com.freecharge.dao.dto.DBSnapshotterDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.model.FileSnapshotAndConfigResponse;

public interface IDBUpdateService {
	public long snapshotUpdate(DBSnapshotterDto dto, FileSnapshotAndConfigResponse response,
			KeyValWithDbOperatorList filterForSnapshotRecords);
	
	public long postDBUpdate(DBPostUploadUpdateDto dto, FileSnapshotAndConfigResponse response) throws SQLException;
}
