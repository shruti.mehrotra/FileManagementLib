package com.freecharge.dao.service;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import com.freecharge.dao.dto.DBQueryDto;
import com.freecharge.dao.dto.DBQueryResponse;
import com.freecharge.model.FileSnapshotAndConfigResponse;

public interface IDBRetrievalService {
	public DBQueryResponse retrieveData(DBQueryDto dto, FileSnapshotAndConfigResponse response);
}
