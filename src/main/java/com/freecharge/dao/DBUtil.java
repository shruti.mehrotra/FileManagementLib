package com.freecharge.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.freecharge.constants.DBConstants;
import com.freecharge.model.FileSnapshotAndConfigResponse;
import com.freecharge.util.DateUtil;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class DBUtil {
	private static final String REPLACEMENT = DBConstants.QUESTION_MARK;

	public static String getDBUrl(String host, int port, String dbName) {
		StringBuilder url = new StringBuilder();

		url.append("jdbc:mysql://").append(host).append(":").append(port).append("/").append(dbName)
				.append("?autoReconnect=true");
		return url.toString();
	}

	public static String getDBUrl(String host, String dbName) {
		return getDBUrl(host, 3306, dbName);
	}

	public static void appendTableNamesToQuery(StringBuilder query, String[] tableNames) {
		for (int i = 0; i < tableNames.length; i++) {
			query.append(tableNames[i]).append(" ");

			if (i != tableNames.length - 1) {
				query.append(", ");
			}
		}
	}

	public static PreparedStatement getPreparedStatement(String sql, FileSnapshotAndConfigResponse response,
			Connection con) throws SQLException {
		PreparedStatement stmt = null;
		if (!StringUtils.isEmpty(sql) && sql.contains(DBConstants.QUESTION_MARK)) {
			Map<String, Object> allowedParamsInQuery = getAllowedConfParamsInQuery(response);
			Map<Integer, Object> sortedParameters = getSortedParametersMap(sql, allowedParamsInQuery);
			sql = getSqlWithoutNamedParams(sql, allowedParamsInQuery, sortedParameters.size());
			stmt = con.prepareStatement(sql);
			int i = 1;
			for (Map.Entry<Integer, Object> entry : sortedParameters.entrySet()) {
				stmt.setObject(i++, entry.getValue());
			}
		} else {
			stmt = con.prepareStatement(sql);
		}

		return stmt;
	}

	private static String getSqlWithoutNamedParams(String sql, Map<String, Object> allowedParamsInQuery, int iterationSize) {
		for (Map.Entry<String, Object> entry : allowedParamsInQuery.entrySet()) {
			if (sql.contains(entry.getKey()) && iterationSize > 0) {
				sql = StringUtils.replace(sql, DBConstants.QUESTION_MARK + entry.getKey(), REPLACEMENT);
				iterationSize--;
			}
		}
		return sql;
	}

	public static Map<Integer, Object> getSortedParametersMap(String sql, Map<String, Object> allowedParamsInQuery) {
		Map<Integer, Object> sortedParameters = new TreeMap<>();
		if (!StringUtils.isEmpty(sql)) {

			for (Map.Entry<String, Object> entry : allowedParamsInQuery.entrySet()) {
				if (sql.contains(entry.getKey())) {
					sql = StringUtils.replace(sql, DBConstants.QUESTION_MARK + entry.getKey(), REPLACEMENT);
					sortedParameters.put(sql.indexOf(entry.getKey()), entry.getValue());
				}
			}
		}

		return sortedParameters;
	}

	public static Map<String, Object> getAllowedConfParamsInQuery(FileSnapshotAndConfigResponse response) {
		Map<String, Object> allowedParamsInQuery = new HashMap<>();
		allowedParamsInQuery.put(DBConstants.FILE_ID, response.getFileSnapshotDto().getFileId());
		allowedParamsInQuery.put(DBConstants.FILE_STATUS, response.getFileSnapshotDto().getFileStatus().name());
		allowedParamsInQuery.put(DBConstants.FILE_TYPE, response.getFileConfigSnapshotDto().getFileConfigType().name());
		allowedParamsInQuery.put(DBConstants.REFUNDED_ON, DateUtil.formatDate(new Date(), DateUtil.YYYYDDMM_HYPHENED_HHMMSS_COLONED));
		return allowedParamsInQuery;
	}
}