package com.freecharge.dao.mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.entity.FileConfigEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 */
@Component
public class FileConfigEntityRepo {

	@Autowired
	private DataSource mysqlDatasource;

	public void insert(FileConfigEntity fileConfigSnapshot) {
		String sql = "INSERT INTO FILE_CONFIG (" + DBConstants.FILE_CONFIG_ID + " , " + DBConstants.FILE_CONFIG_TYPE
				+ " , " + DBConstants.FILE_TEMPLATE + " , " + DBConstants.FILE_PATH_REGEX + ", "
				+ DBConstants.FILE_NAME_REGEX + " , " + DBConstants.FILE_EXTN + ", " + DBConstants.FILE_FLOW_TYPE + ", "
				+ DBConstants.DESTIN_TYPE + ", " + DBConstants.DESTIN_CONFIG_FN + ", " + DBConstants.NOTIFICATION_TYPE
				+ ", " + DBConstants.NOTIFICATION_CONFIG_FN + ", " + DBConstants.CREATE_TS + ", "
				+ DBConstants.UPDATE_TS + " ) VALUES " + " (?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, fileConfigSnapshot.getFileConfigId());
			ps.setString(2, fileConfigSnapshot.getFileConfigType());
			ps.setString(3, fileConfigSnapshot.getFileTemplate());
			ps.setString(4, fileConfigSnapshot.getFilePathRegex());
			ps.setString(5, fileConfigSnapshot.getFileNameRegex());
			ps.setString(6, fileConfigSnapshot.getFileExtension());
			ps.setString(7, fileConfigSnapshot.getFileFlowType());
			ps.setString(8, fileConfigSnapshot.getDestinationType());
			ps.setString(9, fileConfigSnapshot.getDestinationConfigFN());
			ps.setString(10, fileConfigSnapshot.getNotificationType());
			ps.setString(11, fileConfigSnapshot.getNotificationConfig());
			ps.setString(12, fileConfigSnapshot.getCreationTS());
			ps.setString(13, fileConfigSnapshot.getUpdateTS());
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public FileConfigEntity getByFileConfigType(String fileConfigType) {
		FileConfigEntity configEntity = null;
		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_CONFIG WHERE FILE_CONFIG_TYPE = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, fileConfigType);
			ResultSet rs = ps.executeQuery();
			try {
				while (rs.next()) {
					configEntity = getEntityFromRs(rs);
				}
			} catch (SQLException e) {
				return null;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return configEntity;
	}

	public FileConfigEntity getByFileConfigId(long fileConfigId) {
		FileConfigEntity entity = null;
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_CONFIG WHERE FILE_CONFIG_ID = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setLong(1, fileConfigId);
			ResultSet rs = ps.executeQuery();
			try {
				while (rs.next()) {
					entity = getEntityFromRs(rs);
				}
			} catch (SQLException e) {
				return null;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entity;
	}

	private FileConfigEntity getEntityFromRs(ResultSet rs) throws SQLException {
		FileConfigEntity configEntity = new FileConfigEntity();
		configEntity.setFileConfigId(rs.getLong(DBConstants.FILE_CONFIG_ID));
		configEntity.setCreationTS(rs.getString(DBConstants.CREATE_TS));
		configEntity.setDestinationConfigFN(rs.getString(DBConstants.DESTIN_CONFIG_FN));
		configEntity.setDestinationType(rs.getString(DBConstants.DESTIN_TYPE));
		configEntity.setFileConfigType(rs.getString(DBConstants.FILE_CONFIG_TYPE));
		configEntity.setFileExtension(rs.getString(DBConstants.FILE_EXTN));
		configEntity.setFileFlowType(rs.getString(DBConstants.FILE_FLOW_TYPE));
		configEntity.setFileNameRegex(rs.getString(DBConstants.FILE_NAME_REGEX));
		configEntity.setFilePathRegex(rs.getString(DBConstants.FILE_PATH_REGEX));
		configEntity.setFileTemplate(rs.getString(DBConstants.FILE_TEMPLATE));
		configEntity.setNotificationConfig(rs.getString(DBConstants.NOTIFICATION_CONFIG_FN));
		configEntity.setNotificationType(rs.getString(DBConstants.NOTIFICATION_TYPE));
		configEntity.setUpdateTS(rs.getString(DBConstants.UPDATE_TS));

		return configEntity;
	}
}