package com.freecharge.dao.mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.constants.DBConstants;
import com.freecharge.dao.entity.FileManagementEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 */
@Component
public class FileManagementEntityRepo {
	
	@Autowired
	private DataSource mysqlDatasource;
	
	public void insert(FileManagementEntity fileSnapshot) {
		String sql = "INSERT INTO FILE_MANAGEMENT (" + 
				DBConstants.FILE_ID + " , " + 
				DBConstants.FILE_NAME + " , " + 
				DBConstants.FILE_CONFIG_TYPE + " , " + 
				DBConstants.FILE_STATUS + ", " + 
				DBConstants.CREATE_TS + " , " + 
				DBConstants.UPDATE_TS + ", " + 
				DBConstants.FILE_CONFIG_ID + ", " + 
				DBConstants.REC_FILTER_HASH + ", " + 
				DBConstants.NUM_OF_RECORDS + " ) VALUES " + " (?,?,?,?,?,?,?,?,?)";

		
		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, fileSnapshot.getFileId());
			ps.setString(2, fileSnapshot.getFileName());
			ps.setString(3, fileSnapshot.getFileConfigType());
			ps.setString(4, fileSnapshot.getFileStatus());
			ps.setString(5, fileSnapshot.getCreationTS());
			ps.setString(6, fileSnapshot.getUpdateTS());
			ps.setLong(7, fileSnapshot.getFileConfigId());
			ps.setString(8, fileSnapshot.getRecFilterHash());
			ps.setLong(9, fileSnapshot.getNumOfRecords());
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	
	public List<FileManagementEntity> getByFileConfigType(String fileConfigType) {
		List<FileManagementEntity> entityList = new ArrayList<>();
		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_MANAGEMENT WHERE FILE_CONFIG_TYPE = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, fileConfigType);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				try {
					while (rs.next()) {
						entityList.add(getEntityFromRs(rs));
					}
				} catch (SQLException e) {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entityList;
	}
	
	private FileManagementEntity getEntityFromRs(ResultSet rs) throws SQLException {
		FileManagementEntity entity = new FileManagementEntity();
		entity.setCreationTS(rs.getString(DBConstants.CREATE_TS));
		entity.setFileConfigId(rs.getLong(DBConstants.FILE_CONFIG_ID));
		entity.setFileConfigType(rs.getString(DBConstants.FILE_CONFIG_TYPE));
		entity.setFileId(rs.getLong(DBConstants.FILE_ID));
		entity.setFileName(rs.getString(DBConstants.FILE_NAME));
		entity.setFileStatus(rs.getString(DBConstants.FILE_STATUS));
		entity.setNumOfRecords(rs.getLong(DBConstants.NUM_OF_RECORDS));
		entity.setRecFilterHash(rs.getString(DBConstants.REC_FILTER_HASH));
		entity.setUpdateTS(rs.getString(DBConstants.UPDATE_TS));
		return entity;
	}


	public FileManagementEntity getByFileId(long fileId) {
		FileManagementEntity entity = null;
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_MANAGEMENT WHERE FILE_ID = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setLong(1, fileId);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				try {
					while (rs.next()) {
						entity = getEntityFromRs(rs);
					}
				} catch (SQLException e) {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entity;
	}
	
	public void updateStatusByFileId(FileManagementEntity entity) {
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String sql = "UPDATE FILE_MANAGEMENT SET FILE_STATUS = ? WHERE FILE_ID = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, entity.getFileStatus());
			ps.setLong(2, entity.getFileId());
			
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public void updateFileNameAndStatusByFileId(FileManagementEntity entity) {
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String sql = "UPDATE FILE_MANAGEMENT SET FILE_STATUS = ?, FILE_NAME = ?	WHERE FILE_ID = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, entity.getFileStatus());
			ps.setString(2, entity.getFileName());
			ps.setLong(3, entity.getFileId());
			
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public FileManagementEntity getByRecFilterHash(String recFilterHash) {
		FileManagementEntity entity = null;
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_MANAGEMENT WHERE REC_FILTER_HASH = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, recFilterHash);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				try {
					while (rs.next()) {
						entity = getEntityFromRs(rs);
					}
				} catch (SQLException e) {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entity;
	}

	public void updateStatusAndNumOfRecByFileId(FileManagementEntity entity) {
		Connection conn = null;
		try {
			conn = mysqlDatasource.getConnection();
			String sql = "UPDATE FILE_MANAGEMENT SET FILE_STATUS = ?, NUM_OF_RECORDS = ? WHERE FILE_ID = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, entity.getFileStatus());
			ps.setLong(2, entity.getNumOfRecords());
			ps.setLong(3, entity.getFileId());
			
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public List<FileManagementEntity> getByUpdateTsRange(String updateTsStart, 
			String updateTsEnd, String fileConfigType) {
		List<FileManagementEntity> entityList = new ArrayList<>();
		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_MANAGEMENT WHERE UPDATE_TS >= ?"
					+ " AND UPDATE_TS <= ? AND FILE_CONFIG_TYPE=?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, updateTsStart);
			ps.setString(2, updateTsEnd);
			ps.setString(3, fileConfigType);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				try {
					while (rs.next()) {
						entityList.add(getEntityFromRs(rs));
					}
				} catch (SQLException e) {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entityList;
	}
	
	public List<FileManagementEntity> getByConfigTypeWithGenCompleteOrUploadFail(String fileConfigType) {
		List<FileManagementEntity> entityList = new ArrayList<>();
		Connection conn = null;

		try {
			conn = mysqlDatasource.getConnection();
			String query = "SELECT * FROM FILE_MANAGEMENT WHERE "
					+ "FILE_CONFIG_TYPE = ? AND FILE_STATUS IN ('GEN_COMPLETE', 'UPLOAD_FAILED')";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, fileConfigType);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				try {
					while (rs.next()) {
						entityList.add(getEntityFromRs(rs));
					}
				} catch (SQLException e) {
					return null;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return entityList;
	}
}