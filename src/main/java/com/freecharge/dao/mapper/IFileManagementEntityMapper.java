package com.freecharge.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.dao.entity.FileManagementEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */
public interface IFileManagementEntityMapper {
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public void insert(FileManagementEntity fileSnapshot);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileManagementEntity> getAllFileSnapshots();

	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileManagementEntity> getByFileConfigType(String fileConfigType);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileManagementEntity> getByFileConfigTypeAndFileStatus(@Param("fileConfigType") String fileConfigType, @Param("fileStatus") String fileStatus);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public FileManagementEntity getByFileId(long fileId);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public void updateStatusByFileId(FileManagementEntity entity);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public void updateFileNameAndStatusByFileId(FileManagementEntity entity);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public FileManagementEntity getByRecFilterHash(String recFilterHash);

	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public void updateStatusAndNumOfRecByFileId(FileManagementEntity entity);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileManagementEntity> getByUpdateTsRange(@Param("updateTsStart") String updateTsStart, 
			@Param("updateTsEnd") String updateTsEnd, @Param("fileConfigType") String fileConfigType);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileManagementEntity> getByConfigTypeWithGenCompleteOrUploadFail(@Param("fileConfigType") String fileConfigType);
}