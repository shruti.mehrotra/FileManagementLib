package com.freecharge.dao.mapper;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.dao.entity.FileConfigEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 03-Apr-2017
 */
public interface IFileConfigEntityMapper {
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public void insert(FileConfigEntity fileConfigSnapshot);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public List<FileConfigEntity> getAllFileConfigSnapshots();

	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public FileConfigEntity getByFileConfigType(String fileConfigType);
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "mysqlTransactionManager")
	public FileConfigEntity getByFileConfigId(long fileConfigId);
}