package com.fc.payments;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class RefundTransactionDataCreator {

	public static void main(String[] args) {
		String filePath = "/home/standby/Payments/Refunds/sample_refund_txn.csv";
		try {
			/*
			 * transaction_id reference_id idempotency_id refund_amount
			 * refunded_on refund_pg queue_status refund_status updated_on
			 * pg_refund_ref n_last_updated n_created
			 */

			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new FileReader(filePath));
			String[] nextLine;

			nextLine = reader.readNext();
			List<Object[]> data = new ArrayList<>();
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				String txnId = nextLine[0];
				String refId = nextLine[1];
				String idempId = nextLine[2];
				String refundAmt = nextLine[3];
				String refundedOn = nextLine[4];
				String refundPg = nextLine[5];
				String queueStatus = nextLine[6];
				String refundStatus = nextLine[7];
				String updatedOn = nextLine[8];
				String pgRefundRef = nextLine[9];
				String lastUpdated = nextLine[10];
				String created = nextLine[11];
				try {
					Object[] row = { Long.parseLong(txnId), refId, idempId, Double.parseDouble(refundAmt), refundedOn,
							refundPg, queueStatus, Integer.parseInt(refundStatus), updatedOn, pgRefundRef, lastUpdated, created };
					data.add(row);
				} catch (Exception e) {
					System.out.println("Error occured Error: " + e);
				}
			}
			for (Object[] row: data) {
				System.out.println(createInsertStatement(row));
			}
			
		} catch (Exception e) {
			System.out.println("Error occured while parsing Error: " + e);
		}
	}

	public static String createInsertStatement(Object[] row) {
		StringBuilder insertStmt = new StringBuilder();
		insertStmt.append("INSERT INTO refund_transaction VALUES (");
		int count = 0;
		for (Object val: row) {
			if (!(val instanceof Long) && !(val instanceof Double) && !(val instanceof Integer)) {
				insertStmt.append("'")
				.append(val).append("'");
			} else {
				insertStmt.append(val);
			}
			count ++;
			if (count < row.length) {
				insertStmt.append(",");
			}
		}
		insertStmt.append(");");
		return insertStmt.toString();
	}
}
