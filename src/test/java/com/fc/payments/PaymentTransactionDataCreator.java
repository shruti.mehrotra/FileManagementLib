package com.fc.payments;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class PaymentTransactionDataCreator {

	public static void main(String[] args) {
		String filePath = "/home/standby/Payments/Refunds/sample_payment_txn.csv";
		try {
			/*
			 * transaction_id reference_id idempotency_id refund_amount
			 * refunded_on refund_pg queue_status refund_status updated_on
			 * pg_refund_ref n_last_updated n_created
			 */

			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new FileReader(filePath));
			String[] nextLine;

			nextLine = reader.readNext();
			List<Object[]> data = new ArrayList<>();
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				String txnId = nextLine[0];
				String reqId = nextLine[1];
				String responseId = nextLine[2];
				String merchantId = nextLine[3];
				String merchantTxnId = nextLine[4];
				String referenceId = nextLine[5];
				String paymentTypeCode = nextLine[6];
				String cardType = nextLine[7];
				String pgTxnId = nextLine[8];
				String amount = nextLine[9];
				String pgUsed = nextLine[10];
				String status = nextLine[11];
				String createdTime = nextLine[12];
				String updatedTime = nextLine[13];
				String lastUpdated = nextLine[14];
				String created = nextLine[15];
				try {
					Object[] row = { !txnId.contains("NULL") ? Long.parseLong(txnId): txnId, !reqId.contains("NULL") ? Long.parseLong(reqId): reqId, 
							!responseId.contains("NULL") ? Long.parseLong(responseId): responseId, !merchantId.contains("NULL") ? Long.parseLong(merchantId): merchantId,
								merchantTxnId,
							referenceId, paymentTypeCode, cardType, pgTxnId, 
							!amount.contains("NULL") ? Double.parseDouble(amount): amount, pgUsed, !status.contains("NULL") ? Integer.parseInt(status): status, createdTime,
							updatedTime, lastUpdated, created};
					data.add(row);
				} catch (Exception e) {
					System.out.println("Error occured Error: " + e);
				}
			}
			for (Object[] row: data) {
				System.out.println(createInsertStatement(row));
			}
			
		} catch (Exception e) {
			System.out.println("Error occured while parsing Error: " + e);
		}
	}

	public static String createInsertStatement(Object[] row) {
		StringBuilder insertStmt = new StringBuilder();
		insertStmt.append("INSERT INTO payment_transaction VALUES (");
		int count = 0;
		for (Object val: row) {
			if (!(val instanceof Long) && !(val instanceof Double) && !(val instanceof Integer)) {
				insertStmt.append("'")
				.append(val).append("'");
			} else {
				insertStmt.append(val);
			}
			count ++;
			if (count < row.length) {
				insertStmt.append(",");
			}
		}
		insertStmt.append(");");
		return insertStmt.toString();
	}
}
