package com.fc.payments;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class CardBinRangeDataCreator {

	public static void main(String[] args) {
		String filePath = "/home/standby/Payments/Refunds/card_bin_range.csv";
		try {
			/*
			 * transaction_id reference_id idempotency_id refund_amount
			 * refunded_on refund_pg queue_status refund_status updated_on
			 * pg_refund_ref n_last_updated n_created
			 */

			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new FileReader(filePath));
			String[] nextLine;

			nextLine = reader.readNext();
			List<String[]> data = new ArrayList<>();
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				/*String binRangeId = nextLine[0];
				String binTail = nextLine[1];
				String cardType = nextLine[2];
				String cardNature = nextLine[3];
				String status = nextLine[4];*/
				String binHead = nextLine[0];
				String fkCardIssuerId = nextLine[1];
				try {
					String[] row = { "uuid", "*", "RUPAY", "CC", "Active",
							binHead, fkCardIssuerId};
					data.add(row);
				} catch (Exception e) {
					System.out.println("Error occured Error: " + e);
				}
			}
			for (String[] row: data) {
				System.out.println(createInsertStatement(row));
			}
			
		} catch (Exception e) {
			System.out.println("Error occured while parsing Error: " + e);
		}
	}

	public static String createInsertStatement(String[] row) {
		StringBuilder insertStmt = new StringBuilder();
		insertStmt.append("INSERT INTO card_bin_range (bin_range_id, bin_tail, card_type, card_nature, status, bin_head, fk_card_issuer_id) VALUES (");
		int count = 0;
		for (Object val: row) {
			if (!(val instanceof Long) && !(val instanceof Double) && !(val instanceof Integer) && !val.toString().contains("select")) {
				insertStmt.append("'")
				.append(val).append("'");
			} else {
				insertStmt.append(val);
			}
			count ++;
			if (count < row.length) {
				insertStmt.append(",");
			}
		}
		insertStmt.append(");");
		return insertStmt.toString();
	}
}
